package ru.enschin.geomes.util;

import org.springframework.security.crypto.bcrypt.BCrypt;
import ru.enschin.geomes.model.Favorites;
import ru.enschin.geomes.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 02.08.2016.
 */
public class SecureUtil {
    public static final String DEFAULT_HIDDEN_FIELD_STRING = "HIDDEN";
    public static final int DEFAULT_HIDDEN_FIELD_INT = 0;

    private SecureUtil() { }

    //    todo: do it save
    public static String getTokenByLogin(String login) {
        String salt;
        if (login.length() >= 4 && login.length() < 31) {
            salt = BCrypt.gensalt(login.length());
        } else {
            salt = BCrypt.gensalt();
        }
        return mix(login, salt);
    }

    public static void hideFieldsForUserListByFavorites(List<User> userList, Favorites favorites) {
        boolean flag = false;
        for (User user : userList) {
            flag = false;
            if (favorites != null) {
                for (User f : favorites.getFavoriteUsers()) {
                    if (user.getLogin().compareTo(f.getLogin()) == 0) {
                        flag = true;
                        break;
                    }
                }
            }
            hideSecureFields(user);
            if (!flag) {
                hideContactData(user);
            }
        }
    }

    public static void hideFieldsForUserByFavorites(User user, Favorites favorites) {
        List<User> list = new ArrayList<>(1);
        list.add(user);
        hideFieldsForUserListByFavorites(list, favorites);
    }
    public static void hideSecureFields(User user) {
        user.setId(DEFAULT_HIDDEN_FIELD_INT);
        user.setToken(DEFAULT_HIDDEN_FIELD_STRING);
        user.setPassword(DEFAULT_HIDDEN_FIELD_STRING);
    }
    public static void hideContactData(User user) {
        user.seteMail(DEFAULT_HIDDEN_FIELD_STRING);
        user.setFbLink(DEFAULT_HIDDEN_FIELD_STRING);
        user.setTelephoneNumber(DEFAULT_HIDDEN_FIELD_STRING);
        user.setVkLink(DEFAULT_HIDDEN_FIELD_STRING);
    }

    private static String mix(String str1, String str2) {
        int n = str1.length() < str2.length() ? str1.length() : str2.length();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            result.append(str1.charAt(i))
                    .append(str2.charAt(i));
        }
        if (str1.length() != str2.length()) {
            if (str1.length() < str2.length()) {
                result.append(str2.substring(n, str2.length()));
            } else {
                result.append(str1.substring(n, str1.length()));
            }
        }
        return result.toString();
    }


}
