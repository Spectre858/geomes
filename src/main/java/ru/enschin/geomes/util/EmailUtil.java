package ru.enschin.geomes.util;

import org.apache.log4j.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by Andrew on 01.09.2016.
 */
public class EmailUtil {
    public static class DEFAULT_SUBJECTS {
        public static final String USER_REGISTRATION = "Registration at Geomes";
    }
    public static class DEFAULT_MESSAGES {
        public static final String USER_REGISTRATION = "Later here will be the link on which should be go";
    }


    private static final Logger LOGGER = Logger.getLogger(EmailUtil.class.getName());
    private static final String SERVER_ADDRESS = "andrey.enschin@gmail.com";
    private static final String SERVER_PASSWORD = "E+a+b+0109371443";

    private EmailUtil() { }

    public static void sendMessage(String recipient, String subject, String message) throws MessagingException {
        LOGGER.info("Sending message");
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        javax.mail.Session session = javax.mail.Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(SERVER_ADDRESS, SERVER_PASSWORD);
                    }
                });

        Message mes = new MimeMessage(session);
        mes.setFrom(new InternetAddress(SERVER_ADDRESS));
        mes.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(recipient));
        mes.setSubject(subject);
        mes.setText(message);

        Transport.send(mes);

    }
}
