package ru.enschin.geomes.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Andrew on 17.08.2016.
 */
public class DateUtil {
    private static final SimpleDateFormat dateFormat
            = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    public static String getCurrentDateAsString() {
        return dateFormat.format(new Date());
    }
    public static Date getCurrentDate() {
        return new Date();
    }

    public static Date parse(String dateString) {
        Date result = null;
        try {
            result = dateFormat.parse(dateString);
        } catch (ParseException e) {
//            Ignore
        }
        return result;
    }
    public static long getDifference(Date one, Date two) {
        return Math.abs(one.getTime() - two.getTime());
    }


    private DateUtil() { }
}
