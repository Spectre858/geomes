package ru.enschin.geomes.util;

import org.apache.log4j.Logger;
import ru.enschin.geomes.dao.WebSocketUserInfoKeeper;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoNoSuchEntityException;
import ru.enschin.geomes.model.Location;
import ru.enschin.geomes.model.LocationCoords;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.model.WebSocketUserInfo;

import javax.websocket.Session;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Andrew on 03.08.2016.
 */
public class WebSocketUserInfoHandler {
    private static final Logger LOGGER = Logger
            .getLogger(WebSocketUserInfoHandler.class.getName());

    @Deprecated
    private static final Map<String, Map<String, Session>> SESSIONS
            = new ConcurrentHashMap<String, Map<String, Session>>();
    private static final WebSocketUserInfoKeeper keeper = new WebSocketUserInfoKeeper();

    private WebSocketUserInfoHandler() { }

    @Deprecated
    public static void addSession(String location, String login, Session session) {
        LOGGER.info(String.format("addSession(%s, %s, session)", location, login));
        Map<String, Session> place = SESSIONS.get(location);

        if (place == null) {
            place = new ConcurrentHashMap<String, Session>();
            synchronized (SESSIONS) {
                if (!SESSIONS.containsKey(location)) {
                    SESSIONS.put(location, place);
                }
            }
        }
        place.put(login, session);
    }

    public static void addWebSocketUserInfo(User user, Location location, Session session) throws DaoBusinessException {
        LOGGER.info(String.format("addWebSocketUserInfo(%s)", user.getLogin()));
        keeper.addWebSocketUserInfo(new WebSocketUserInfo(user, location, session));
    }

    public static void updateCoords(Location location, String userName, LocationCoords newCoords) throws DaoNoSuchEntityException {
        LOGGER.info("Update coords");
        keeper.updateCoords(location, userName, newCoords);
    }

    @Deprecated
    public static void removeSession(String location, String login) {
        LOGGER.info(String.format("removeSession(%s, %s)", location, login));
        Map<String, Session> place = SESSIONS.get(location);

        try {
            if (place != null && place.containsKey(login)) {
                place.remove(login);
            }
        } catch (NullPointerException e) {
//            Ignore
        }
    }
    public static void removeWebSocketUserInfo(Location location, String login) {
        LOGGER.info(String.format("removeWebSocketUserInfo(%s)", login));
        keeper.removeWebSocketUserInfo(location, login);
    }

    @Deprecated
    public static void sendMessage(String location, String login, String message) throws IOException {
        LOGGER.info(String.format("sendMessage(%s, %s, message)", location, login));
        SESSIONS.get(location).get(login).getBasicRemote().sendText(message);
    }

    public static void sendMessage(Location location, String destinationUserLogin, String message) throws DaoNoSuchEntityException, IOException {
        LOGGER.info(String.format("sendMessage to %s", destinationUserLogin));
        WebSocketUserInfo userInfo = keeper.getWebSocketUserInfo(location, destinationUserLogin);
        userInfo.getSession().getBasicRemote().sendText(message);
    }


    @Deprecated
    public static Set<String> getSetOfUsersByLocation(String location) {
        LOGGER.info(String.format("getSetOfUserByLocation(%s)", location));
        Map<String, Session> place = SESSIONS.get(location);

        if (place == null) {
            return null;
        }
        return place.keySet();
    }
    public static List<WebSocketUserInfo> getListOfWebSocketUserInfoInCity(String country, String city) {
        LOGGER.info(String.format("getListOfUserInCity(%s)", city));
        Location location = new Location(country, city, null, null);
        List<WebSocketUserInfo> result =  keeper.getListOfWebSocketUserInfoInCity(location);
        for (WebSocketUserInfo ws : result) {
            ws.setSession(null);
        }
        return result;
    }

    public static List<WebSocketUserInfo> getListOfWebSocketUserInfoAtPlace(String country, String city, String place) {
        LOGGER.info(String.format("getListOfUserAtPlace(%s)", place));
        Location location = new Location(country, city, place, null);
        List<WebSocketUserInfo> result = keeper.getListOfWebSocketUserInfoAtPlace(location);
        for (WebSocketUserInfo ws : result) {
            ws.setSession(null);
        }
        return result;
    }

    public static List<WebSocketUserInfo> getListOfWebSocketUserInfoAtPlace(Location location) {
        return getListOfWebSocketUserInfoAtPlace(location.getCountry(), location.getCity(), location.getPlace());
    }

    @Deprecated
    public static boolean checkCompliance(String location, String login) {
        LOGGER.info(String.format("checkCompliance(%s, %s)", location, login));
        Map<String, Session> place = SESSIONS.get(location);

        if (place == null) {
            return false;
        }
        if (!place.containsKey(login)) {
            return false;
        }
        return true;
    }
    public static boolean checkCompliance(Location location, String userName) {
        LOGGER.info(String.format("checkCompliance(%s)", userName));
        return keeper.contains(location, userName);
    }

}
