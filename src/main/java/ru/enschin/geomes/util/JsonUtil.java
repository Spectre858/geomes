package ru.enschin.geomes.util;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import ru.enschin.geomes.model.*;
import ru.enschin.geomes.model.Error;

import java.io.IOException;
import java.util.List;

/**
 * Created by Andrew on 02.08.2016.
 */
public class JsonUtil {

    private JsonUtil() { }

    public static String userToJson(User user) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(user);
    }

    public static String getErrorJson(Error.Type type) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(new Error(type));
    }
    public static String getSuccessJson() {
        return "{\"type\":\"success\"}";
    }

    public static WebSocketMessage jsonToWebSocketMessage(String messageJson) throws IOException {
        return new ObjectMapper().readValue(messageJson, WebSocketMessage.class);
    }

    public static String webSocketMessageToJson(WebSocketMessage message) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(message);
    }

    public static String locationToJson(Location location) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(location);
    }
    public static String tokenToJson(String token) {
        return String.format("{\"token\":\"%s\"}", token);
    }

    public static String userListToJson(List<User> users) throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        ObjectNode node = om.createObjectNode();
        ArrayNode arrayNode = node.putArray("users");

        for (User u : users) {
            arrayNode.addPOJO(u);
        }
        return om.writeValueAsString(node);
    }
    public static String webSocketUserInfoToJson(List<WebSocketUserInfo> userInfoList) throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        ObjectNode node = om.createObjectNode();
        ArrayNode arrayNode = node.putArray("users");

        for (WebSocketUserInfo user : userInfoList) {
            arrayNode.addPOJO(user);
        }
        return om.writeValueAsString(node);
    }

    public static String likeToJson(Like like) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(like);
    }
    public static String favoritesToJson(Favorites favorites) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(favorites);
    }

}
