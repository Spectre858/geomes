package ru.enschin.geomes.util;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletContext;

/**
 * Created by Andrew on 06.08.2016.
 */
public class ApplicationContextUtil {
    private static final Logger LOGGER = Logger.getLogger(ApplicationContextUtil.class.getName());
    private static final String APP_CTX_PATH = "contextConfigLocation";
    private static final String DEFAULT_CONFIG_LOCATION = "applicationContext.xml";
    private static ApplicationContext appCtx = null;

    public synchronized static ApplicationContext getInstanceOfApplicationContext(ServletContext servletContext) throws Exception {
        if (appCtx == null) {
            String configLocation = servletContext.getInitParameter(APP_CTX_PATH);
            LOGGER.info("load" + APP_CTX_PATH + " -> " + configLocation);

            if (configLocation == null) {
                LOGGER.warn("ERROR: need init param " + APP_CTX_PATH);
                throw new Exception(APP_CTX_PATH + " init param == null");
            }

            appCtx = new ClassPathXmlApplicationContext(configLocation);
        }
        return appCtx;
    }

    private ApplicationContextUtil() { }

    public synchronized static ApplicationContext getDefaultInstanceOfApplicationContext() {
        if (appCtx == null) {
            appCtx = new ClassPathXmlApplicationContext(DEFAULT_CONFIG_LOCATION);
        }
        return appCtx;
    }
}
