package ru.enschin.geomes.util;

import ru.enschin.geomes.model.User;
import ru.enschin.geomes.model.WebSocketMessage;

/**
 * Validator of some stuff (sqlinjection, http stuff, invalid fields types)
 * todo: do it
 */
public class ValidateUtil {

    private ValidateUtil() { }

    /**
     * Validate the {@param user} by correct data in fields.
     */
    public static boolean validateOfUser(User user) {

//        check the null pointer
        if (user.getToken() == null
                || user.getLogin() == null
                || user.getPassword() == null
                || user.getFullName() == null
                || user.getAge() < 0
                || user.geteMail() == null) {
            return false;
        }
        return true;
    }

    public static boolean isJson(String string) {
        if (string.startsWith("{") && string.endsWith("}")) {
            return true;
        }
        return false;
    }

    public static boolean validateOfWebSocketMessage(WebSocketMessage message) {

//        check the null pointer
        if (message.getType() == null
                || message.getDestinationUser() == null
                || message.getSourceUser() == null
                || message.getMessage() == null) {
            return false;
        }
        return true;
    }

    /**
        Validation of {@param line} by correct data:
            - null-value
            - sql
            - size
     */
    public static boolean fullValidation(String line) {

//        check the null pointer

        if (line == null) {
            return false;
        }

        /*
            some validation
         */
        return true;
    }
}
