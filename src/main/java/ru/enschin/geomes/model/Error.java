package ru.enschin.geomes.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Andrew on 02.08.2016.
 */
public class Error implements Serializable {
    public enum Type {
        INVALID_REQUEST_PARAMETERS,
        INVALID_EMAIL,
        INVALID_AUTHORIZATION,
        NO_SUCH_ENTITY,
        DISCREPANCY_OF_LOCATION,
        SYSTEM_ERROR,
        INVALID_REQUEST_TYPE,
        ENTITY_ALREADY_EXIST
    }

    @JsonProperty("type")
    private Type type;

    public Error() {
    }

    public Error(Type type) {
        this.type = type;
    }

    public void setType(Type type) {

        this.type = type;
    }

    public Type getType() {

        return type;
    }

}
