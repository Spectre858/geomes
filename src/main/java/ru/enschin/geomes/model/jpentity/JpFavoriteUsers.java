package ru.enschin.geomes.model.jpentity;

import ru.enschin.geomes.model.Favorites;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.util.SecureUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 23.08.2016.
 */
@Entity
@Table(name = "jp_favorite_users")
@Cacheable
@Access(AccessType.FIELD)
public class JpFavoriteUsers implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "favorites_id")
    private int id;
    @Column(name = "count")
    private int count;
    @OneToOne(mappedBy = "favoriteUsers")
    private JpUserSystemInfo ownerSystemInfo;
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(name = "favorites_join_user",
            joinColumns = { @JoinColumn(name = "favorites_id", referencedColumnName = "favorites_id")},
            inverseJoinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "user_id")})
    private List<JpUser> userList;

    public JpFavoriteUsers() {
    }

    public JpFavoriteUsers(int count, JpUserSystemInfo ownerSystemInfo, List<JpUser> userList) {
        this.count = count;
        this.ownerSystemInfo = ownerSystemInfo;
        this.userList = userList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public JpUserSystemInfo getOwnerSystemInfo() {
        return ownerSystemInfo;
    }

    public void setOwnerSystemInfo(JpUserSystemInfo ownerSystemInfo) {
        this.ownerSystemInfo = ownerSystemInfo;
    }

    public List<JpUser> getUserList() {
        return userList;
    }

    public void setUserList(List<JpUser> userList) {
        this.userList = userList;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Favorites toFavorites() {
        List<User> favoriteList = new ArrayList<>();
        User user = null;
        for (JpUser tmp : userList) {
            user = tmp.toUser();
            user.setId(SecureUtil.DEFAULT_HIDDEN_FIELD_INT);
            user.setPassword(SecureUtil.DEFAULT_HIDDEN_FIELD_STRING);
            user.setToken(SecureUtil.DEFAULT_HIDDEN_FIELD_STRING);
            favoriteList.add(user);
        }
        return new Favorites(ownerSystemInfo.getUser().getLogin(), count, favoriteList);
    }
}
