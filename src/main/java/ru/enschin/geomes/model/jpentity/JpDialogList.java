package ru.enschin.geomes.model.jpentity;

import ru.enschin.geomes.model.DialogInfo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Andrew on 17.08.2016.
 */
@Entity
@Table(name = "jp_dialog_list")
@Cacheable
@Access(AccessType.FIELD)
public class JpDialogList implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dialog_id")
    private int id;

    @Column(name = "location")
    private String location;
    @Column(name = "interlocutor")
    private String interlocutor;
    @Column(name = "last_message_time")
    private String lastMessageTime;
    @Column(name = "count_message")
    private int countMessage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "system_info_id")
    private JpUserSystemInfo systemInfo;

    public JpDialogList() {
    }

    public JpDialogList(String location, String interlocutor, String lastMessageTime, int countMessage, JpUserSystemInfo systemInfo) {
        this.location = location;
        this.interlocutor = interlocutor;
        this.lastMessageTime = lastMessageTime;
        this.countMessage = countMessage;
        this.systemInfo = systemInfo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getInterlocutor() {
        return interlocutor;
    }

    public void setInterlocutor(String interlocutor) {
        this.interlocutor = interlocutor;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(String lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public JpUserSystemInfo getSystemInfo() {
        return systemInfo;
    }

    public void setSystemInfo(JpUserSystemInfo systemInfo) {
        this.systemInfo = systemInfo;
    }

    public int getCountMessage() {
        return countMessage;
    }

    public void setCountMessage(int countMessage) {
        this.countMessage = countMessage;
    }

    public DialogInfo toDialogInfo() {
        return new DialogInfo(location, systemInfo.getUser().getLogin(), interlocutor, countMessage, lastMessageTime);
    }
}
