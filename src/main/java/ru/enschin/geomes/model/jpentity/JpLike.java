package ru.enschin.geomes.model.jpentity;

import ru.enschin.geomes.model.Like;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.util.SecureUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 20.08.2016.
 */
@Entity
@Table(name = "jp_like")
@Cacheable
@Access(AccessType.FIELD)
public class JpLike implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "like_id")
    private int id;
    @Column(name = "count")
    private long count;
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "likes")
    private JpUserSystemInfo ownerSystemInfo;
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(name = "like_join_user",
        joinColumns = { @JoinColumn(name = "like_id", referencedColumnName = "like_id")},
        inverseJoinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "user_id")})
    private List<JpUser> userList;

    public JpLike() {
    }

    public JpLike(long count, JpUserSystemInfo ownerSystemInfo, List<JpUser> userList) {
        this.count = count;
        this.ownerSystemInfo = ownerSystemInfo;
        this.userList = userList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public JpUserSystemInfo getOwnerSystemInfo() {
        return ownerSystemInfo;
    }

    public void setOwnerSystemInfo(JpUserSystemInfo ownerSystemInfo) {
        this.ownerSystemInfo = ownerSystemInfo;
    }

    public List<JpUser> getUserList() {
        return userList;
    }

    public void setUserList(List<JpUser> userList) {
        this.userList = userList;
    }

    public Like toLike() {
        List<User> likes = new ArrayList<User>();
        for (JpUser user : userList) {
            User us = user.toUser();
            us.setId(SecureUtil.DEFAULT_HIDDEN_FIELD_INT);
            us.setPassword(SecureUtil.DEFAULT_HIDDEN_FIELD_STRING);
            us.setToken(SecureUtil.DEFAULT_HIDDEN_FIELD_STRING);
            likes.add(us);
        }
        return new Like(ownerSystemInfo.getUser().getLogin(), count, likes);
    }
}
