package ru.enschin.geomes.model.jpentity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Andrew on 17.08.2016.
 */
@Entity
@Table(name = "jp_user_system_info")
@Cacheable
@Access(AccessType.FIELD)
public class JpUserSystemInfo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "system_info_id")
    private int id;
    @Column(name = "regustration_date")
    private String registrationDate;

    @OneToMany(mappedBy = "systemInfo", cascade = {CascadeType.ALL})
    private List<JpDialogList> dialogList;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "systemInfo")
    private JpUser user;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "like_id")
    private JpLike likes;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "favorites_id")
    private JpFavoriteUsers favoriteUsers;

    public JpUserSystemInfo() {
    }

    public JpUserSystemInfo(String registrationDate, List<JpDialogList> dialogList,
                            JpUser user, JpLike likes, JpFavoriteUsers favoriteUsers) {
        this.registrationDate = registrationDate;
        this.dialogList = dialogList;
        this.user = user;
        this.likes = likes;
        this.favoriteUsers = favoriteUsers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public List<JpDialogList> getDialogList() {
        return dialogList;
    }

    public void setDialogList(List<JpDialogList> dialogList) {
        this.dialogList = dialogList;
    }

    public JpUser getUser() {
        return user;
    }

    public void setUser(JpUser user) {
        this.user = user;
    }

    public JpLike getLikes() {
        return likes;
    }

    public void setLikes(JpLike likes) {
        this.likes = likes;
    }

    public JpFavoriteUsers getFavoriteUsers() {
        return favoriteUsers;
    }

    public void setFavoriteUsers(JpFavoriteUsers favoriteUsers) {
        this.favoriteUsers = favoriteUsers;
    }
}
