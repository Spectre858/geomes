package ru.enschin.geomes.model.jpentity;

import ru.enschin.geomes.model.User;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Andrew on 13.08.2016.
 */
@Entity
@Table(name = "jp_user_info")
@Cacheable
@Access(AccessType.FIELD)
public class JpUserInfo implements Serializable {
    @Id
    @Column(name = "info_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "mood")
    private String mood;
    @Column(name = "full_name", nullable = false)
    private String fullName;
    @Column(name = "age", nullable = false)
    private int age;
    @Column(name = "e_mail", unique = true, nullable = false)
    private String eMail;
    @Column(name = "profile_picture_link")
    private String profilePictureLink;
    @Column(name = "telephone_number")
    private String telephoneNumber;
    @Column(name = "vk_link")
    private String vkLink;
    @Column(name = "fb_link")
    private String fbLink;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "userInfo")
    private JpUser jpUser;

    public JpUserInfo() {
    }

    public JpUserInfo(String mood, String fullName, int age, String eMail, String profilePictureLink, String telephoneNumber, String vkLink, String fbLink, JpUser jpUser) {
        this.mood = mood;
        this.fullName = fullName;
        this.age = age;
        this.eMail = eMail;
        this.profilePictureLink = profilePictureLink;
        this.telephoneNumber = telephoneNumber;
        this.vkLink = vkLink;
        this.fbLink = fbLink;
        this.jpUser = jpUser;
    }

    public JpUser getJpUser() {
        return jpUser;
    }

    public void setJpUser(JpUser jpUser) {
        this.jpUser = jpUser;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getProfilePictureLink() {
        return profilePictureLink;
    }

    public void setProfilePictureLink(String profilePictureLink) {
        this.profilePictureLink = profilePictureLink;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getVkLink() {
        return vkLink;
    }

    public void setVkLink(String vkLink) {
        this.vkLink = vkLink;
    }

    public String getFbLink() {
        return fbLink;
    }

    public void setFbLink(String fbLink) {
        this.fbLink = fbLink;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User toUser() {
        return new User(null, null, null, mood, fullName, age, eMail, profilePictureLink,
                telephoneNumber, vkLink, fbLink);
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }
}
