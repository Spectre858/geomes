package ru.enschin.geomes.model.jpentity;

import ru.enschin.geomes.model.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Andrew on 13.08.2016.
 */
@Entity
@Table(name = "jp_users")
@Cacheable
@Access(AccessType.FIELD)
public class JpUser implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;
    @Column(name = "token", unique = true, nullable = false)
    private String token;
    @Column(name = "login", unique = true, nullable = false)
    private String login;
    @Column(name = "password", nullable = false)
    private String password;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "info_id", nullable = false)
    private JpUserInfo userInfo;

    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinColumn(name = "system_info_id")
    private JpUserSystemInfo systemInfo;

    public JpUser() {
    }

    public JpUser(String token, String login, String password, JpUserInfo userInfo, JpUserSystemInfo systemInfo) {
        this.token = token;
        this.login = login;
        this.password = password;
        this.userInfo = userInfo;
        this.systemInfo = systemInfo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserInfo(JpUserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public int getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public JpUserInfo getUserInfo() {
        return userInfo;
    }

    public User toUser() {
        return new User(id, token, login, password, userInfo.getMood(), userInfo.getFullName(), userInfo.getAge(),
                userInfo.geteMail(), userInfo.getProfilePictureLink(), userInfo.getTelephoneNumber(),
                userInfo.getVkLink(), userInfo.getFbLink());
    }

    public void setAllFields(User user, String registrationDate, List<JpDialogList> dialogList, JpLike likes, JpFavoriteUsers favoriteUsers) {
        token = user.getToken();
        login = user.getLogin();
        password = user.getPassword();
        if (userInfo == null) {
            userInfo = new JpUserInfo();
            userInfo.setJpUser(this);
        }
        userInfo.setFullName(user.getFullName());
        userInfo.setAge(user.getAge());
        userInfo.seteMail(user.geteMail());
        userInfo.setProfilePictureLink(user.getProfilePictureLink());
        userInfo.setTelephoneNumber(user.getTelephoneNumber());
        userInfo.setVkLink(user.getVkLink());
        userInfo.setFbLink(user.getFbLink());

        if (systemInfo == null) {
            systemInfo = new JpUserSystemInfo();
            systemInfo.setUser(this);
        }
        systemInfo.setRegistrationDate(registrationDate);
        systemInfo.setDialogList(dialogList);
        systemInfo.setLikes(likes);
        systemInfo.setFavoriteUsers(favoriteUsers);
    }


    public JpUserSystemInfo getSystemInfo() {
        return systemInfo;
    }

    public void setSystemInfo(JpUserSystemInfo systemInfo) {
        this.systemInfo = systemInfo;
    }
}
