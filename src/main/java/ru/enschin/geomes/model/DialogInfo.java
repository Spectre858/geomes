package ru.enschin.geomes.model;

import java.io.Serializable;

/**
 * Created by Andrew on 17.08.2016.
 */
public class DialogInfo implements Serializable {
    private String location;
    private String owner;
    private String interlocutor;
    private int countMessage;
    private String lastMessageTime;

    public DialogInfo() {
    }

    public DialogInfo(String location, String owner, String interlocutor, int countMessage, String lastMessageTime) {
        this.location = location;
        this.owner = owner;
        this.interlocutor = interlocutor;
        this.countMessage = countMessage;
        this.lastMessageTime = lastMessageTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getInterlocutor() {
        return interlocutor;
    }

    public void setInterlocutor(String interlocutor) {
        this.interlocutor = interlocutor;
    }

    public int getCountMessage() {
        return countMessage;
    }

    public void setCountMessage(int countMessage) {
        this.countMessage = countMessage;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(String lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }
}
