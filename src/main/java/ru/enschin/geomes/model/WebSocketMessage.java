package ru.enschin.geomes.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Andrew on 03.08.2016.
 */
public class WebSocketMessage implements Serializable {
    public static final String SOURCE_USER_SERVER = "SERVER";
    public static final String DESTINATION_USER_DEFAULT = "User";
    public enum Type {
        MESSAGE,
        UPDATE_COORDS,
        LIKE,
        ADDITION_IN_FAVORITE,
        INVALID_REQUEST_PARAMETERS,
        INVALID_MESSAGE_ERROR,
        NO_SUCH_DESTINATION_ERROR,
        LIMIT_MESSAGE_ERROR,
        INVALID_AUTHORIZATION_ERROR,
        SYSTEM_ERROR
    }

    @JsonProperty("type")
    private WebSocketMessage.Type type;
    @JsonProperty("destination_user")
    private String destinationUser;
    @JsonProperty("source_user")
    private String sourceUser;
    @JsonProperty("message")
    private String message;

    public WebSocketMessage() {
    }

    public WebSocketMessage(Type type, String destinationUser, String sourceUser, String message) {
        this.type = type;
        this.destinationUser = destinationUser;
        this.sourceUser = sourceUser;
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public String getDestinationUser() {
        return destinationUser;
    }

    public String getMessage() {
        return message;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setDestinationUser(String destinationUser) {
        this.destinationUser = destinationUser;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSourceUser() {
        return sourceUser;
    }

    public void setSourceUser(String sourceUser) {
        this.sourceUser = sourceUser;
    }
}
