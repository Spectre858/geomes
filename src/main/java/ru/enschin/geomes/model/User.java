package ru.enschin.geomes.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Model of users
 */
public class User implements Serializable{
//    user table
    @JsonProperty("id")
    private int id;
    @JsonProperty("token")
    private String token;
    @JsonProperty("login")
    private String login;
    @JsonProperty("password")
    private String password;
//    user_info table
    @JsonProperty("mood")
    private String mood;
    @JsonProperty("full_name")
    private String fullName;
    @JsonProperty("age")
    private int age;
    @JsonProperty("e_mail")
    private String eMail;
    @JsonProperty("profile_picture_link")
    private String profilePictureLink;
    @JsonProperty("telephone_number")
    private String telephoneNumber;
    @JsonProperty("vk_link")
    private String vkLink;
    @JsonProperty("fb_link")
    private String fbLink;

    public User() { }

    public User(String token, String login, String password) {
        this.token = token;
        this.login = login;
        this.password = password;
    }

    //  Constructor for user without info
    public User(int id, String token, String login, String password) {
        this.id = id;
        this.token = token;
        this.login = login;
        this.password = password;
    }

    public User(String token, String login, String password, String mood, String fullName,
                int age, String eMail, String profilePictureLink, String telephoneNumber, String vkLink, String fbLink) {
        this.token = token;
        this.login = login;
        this.password = password;
        this.mood = mood;
        this.fullName = fullName;
        this.age = age;
        this.eMail = eMail;
        this.profilePictureLink = profilePictureLink;
        this.telephoneNumber = telephoneNumber;
        this.vkLink = vkLink;
        this.fbLink = fbLink;
    }

    //  Constructor for user with info
    public User(int id, String token, String login, String password, String mood, String fullName,
                int age, String eMail, String profilePictureLink, String telephoneNumber, String vkLink, String fbLink) {
        this.id = id;
        this.token = token;
        this.login = login;
        this.password = password;
        this.mood = mood;
        this.fullName = fullName;
        this.age = age;
        this.eMail = eMail;
        this.profilePictureLink = profilePictureLink;
        this.telephoneNumber = telephoneNumber;
        this.vkLink = vkLink;
        this.fbLink = fbLink;
    }

    public int getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getFullName() {
        return fullName;
    }

    public int getAge() {
        return age;
    }

    public String geteMail() {
        return eMail;
    }

    public String getProfilePictureLink() {
        return profilePictureLink;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public String getVkLink() {
        return vkLink;
    }

    public String getFbLink() {
        return fbLink;
    }

    public void setId(int id) {

        this.id = id;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public void setProfilePictureLink(String profilePictureLink) {
        this.profilePictureLink = profilePictureLink;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public void setVkLink(String vkLink) {
        this.vkLink = vkLink;
    }

    public void setFbLink(String fbLink) {
        this.fbLink = fbLink;
    }

    @Override
    public String toString() {
        return "User:\n" +
                "{\n" +
                "\tid: " + id + "\n" +
                "\ttoken: " + token + "\n" +
                "\tlogin: " + login + "\n" +
                "\tpassword: " + password + "\n" +
                "\tfull_name: " + fullName + "\n" +
                "\tage: " + age + "\n" +
                "\te_mail: " + eMail + "\n" +
                "\tprofile_picture_link: " + profilePictureLink + "\n" +
                "\tvk_link: " + vkLink + "\n" +
                "\tfb_link: " + fbLink + "\n" +
                "}";
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }
}
