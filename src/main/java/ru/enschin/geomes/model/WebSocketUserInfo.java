package ru.enschin.geomes.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.websocket.Session;

/**
 * Created by Andrew on 29.08.2016.
 */
public class WebSocketUserInfo {
    @JsonProperty("user")
    private User user;
    @JsonProperty("location")
    private Location location;
    @JsonIgnore
    private Session session;

    public WebSocketUserInfo() {
    }

    public WebSocketUserInfo(User user, Location location, Session session) {
        this.user = user;
        this.location = location;
        this.session = session;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
