package ru.enschin.geomes.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import ru.enschin.geomes.dao.DaoManager;
import ru.enschin.geomes.dao.MysqlDaoManager;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Set;

/**
 * Created by Andrew on 06.08.2016.
 */
public class Location implements Serializable {
    @JsonProperty("country")
    private String country;
    @JsonProperty("city")
    private String city;
    @JsonProperty("place")
    private String place;
    @JsonProperty("coords")
    private LocationCoords coords;

    public Location() {
    }

    public Location(String country, String city, String place, LocationCoords coords) {
        this.country = country;
        this.city = city;
        this.place = place;
        this.coords = coords;
    }

    public static Location createFromWebSocketFormat(String place, String coords) {
        String[] splitPlace = place.split("\\.");
        if (splitPlace.length < 2 || splitPlace.length > 3) {
            return null;
        }
        String country = splitPlace[0];
        String city = splitPlace[1];
        String local = splitPlace.length == 3 ? splitPlace[2] : null;

        String[] splitCoords = coords.split("-");
        if (splitCoords.length != 2) {
            return null;
        }
        double longitude;
        double latitude;
        try {
            longitude = Double.valueOf(splitCoords[0]);
            latitude = Double.valueOf(splitCoords[1]);
        } catch (NumberFormatException | NullPointerException e) {
            return null;
        }

        return new Location(country, city, local, new LocationCoords(longitude, latitude));
    }

    public static Location createByPlace(String place) {
        String[] splitPlace = place.split("\\.");
        if (splitPlace.length < 2 || splitPlace.length > 3) {
            return null;
        }
        String country = splitPlace[0];
        String city = splitPlace[1];
        String local = splitPlace.length == 3 ? splitPlace[2] : null;

        return new Location(country, city, local, new LocationCoords(0, 0));
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public LocationCoords getCoords() {
        return coords;
    }

    public void setCoords(LocationCoords coords) {
        this.coords = coords;
    }
}
