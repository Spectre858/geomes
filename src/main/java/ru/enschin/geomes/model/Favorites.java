package ru.enschin.geomes.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Andrew on 23.08.2016.
 */
public class Favorites implements Serializable {
    @JsonProperty("owner")
    private String owner;
    @JsonProperty("count")
    private long count;
    @JsonProperty("favorite_user_list")
    private List<User> favoriteUsers;

    public Favorites() {
    }

    public Favorites(String owner, long count, List<User> favoriteUsers) {
        this.owner = owner;
        this.count = count;
        this.favoriteUsers = favoriteUsers;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<User> getFavoriteUsers() {
        return favoriteUsers;
    }

    public void setFavoriteUsers(List<User> favoriteUsers) {
        this.favoriteUsers = favoriteUsers;
    }
}
