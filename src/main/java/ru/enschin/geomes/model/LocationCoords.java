package ru.enschin.geomes.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Andrew on 29.08.2016.
 */
public class LocationCoords {
    @JsonProperty("longitude")
    private double longitude;
    @JsonProperty("latitude")
    private double latitude;

    public LocationCoords() {
    }

    public LocationCoords(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public static LocationCoords createFromWebSocketFormat(String coords) {
        String[] splitCoords = coords.split("-");
        if (splitCoords.length != 2) {
            return null;
        }
        double longitude;
        double latitude;
        try {
            longitude = Double.valueOf(splitCoords[0]);
            latitude = Double.valueOf(splitCoords[1]);
        } catch (NumberFormatException | NullPointerException e) {
            return null;
        }
        return new LocationCoords(longitude, latitude);
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
