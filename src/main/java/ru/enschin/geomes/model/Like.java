package ru.enschin.geomes.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Andrew on 20.08.2016.
 */
public class Like implements Serializable {
    @JsonProperty("owner")
    private String owner;
    @JsonProperty("count_likes")
    private long countLikes;
    @JsonProperty("adoring_user_list")
    private List<User> adoringUsers;

    public Like() {
    }

    public Like(String owner, long countLikes, List<User> adoringUsers) {
        this.owner = owner;
        this.countLikes = countLikes;
        this.adoringUsers = adoringUsers;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getCountLikes() {
        return countLikes;
    }

    public void setCountLikes(long countLikes) {
        this.countLikes = countLikes;
    }

    public List<User> getAdoringUsers() {
        return adoringUsers;
    }

    public void setAdoringUsers(List<User> adoringUsers) {
        this.adoringUsers = adoringUsers;
    }
}
