package ru.enschin.geomes.dao.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.util.SecureUtil;

import java.sql.SQLException;

/**
 * Created by Andrew on 01.08.2016.
 */
public class DaoTest {
    private static final Double RADIUS_IN_RANGE = 0.00299574131;
    public static void main(String[] args) throws DaoSystemException, DaoBusinessException, SQLException, JsonProcessingException {

        System.out.println(SecureUtil.getTokenByLogin("spectre858"));
//            55.035292, 82.925143
//            55.032371, 82.925808

//        boolean result = inRange(new LocationCoords(55.035292, 82.925143),
//                new LocationCoords(55.032371, 82.925808));
//        System.out.println(result);
//        Double result = Math.sqrt(
//                Math.pow(Math.abs(3 - 0),2)
//                        + Math.pow(Math.abs(3 - 0), 2));
//        System.out.println(result);
//        Location location = Location.createFromWebSocketFormat("Russia.Novosibirsk.Aura", "54.642332-66.32342");
//        if (location == null) {
//            System.out.println("is NULL");
//        } else {
//            System.out.println(location.getPlace() + location.getCoords().getLatitude());
//        }
//        WebSocketUserInfoKeeper keeper = new WebSocketUserInfoKeeper();
//        User user1 = new User(GenerateUtil
//                .getTokenByLogin("goga"), "goga", "1234", "Goga",
//                20, "goga@ya.ru", "blabla/myimage.jpg", "9833233668",
//                "my/Vk/Link", "my/Fb/Link");
//        User user2 = new User(GenerateUtil
//                .getTokenByLogin("test"), "test", "1234", "Test",
//                20, "test@ya.ru", "blabla/myimage.jpg", "9833233668",
//                "my/Vk/Link", "my/Fb/Link");
//        Location location = new Location("Russia", "Novosibirsk", "Aura", new LocationCoords(50.444334, 43.344235));
//        Location location1 = new Location("Russia", "Novosibirsk", null, new LocationCoords(50.444334, 43.344235));
//
//        WebSocketUserInfo webSocketUserInfo = new WebSocketUserInfo(user1, location, null);
//        keeper.addWebSocketUserInfo(webSocketUserInfo);
//        System.out.println("added");
//        webSocketUserInfo = new WebSocketUserInfo(user2, location1, null);
//        keeper.addWebSocketUserInfo(webSocketUserInfo);
//        System.out.println("added");
//        WebSocketUserInfo result = keeper.getWebSocketUserInfo(location, user1.getLogin());
//        System.out.println(result.getUser() + " " + result.getLocation().getCoords());
//        List<WebSocketUserInfo> rs = keeper.getListOfWebSocketUserInfoAtPlace(location1);
//        for (WebSocketUserInfo ws : rs ) {
//            System.out.println(ws.getUser());
//        }


//        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
//        DaoManager daoManager = applicationContext.getBean(JpaDaoManager.class);

//        daoManager.createUser(
//                new User(GenerateUtil
//                        .getTokenByLogin("goga"), "goga", "1234", "Goga",
//                        20, "goga@ya.ru", "blabla/myimage.jpg", "9833233668",
//                        "my/Vk/Link", "my/Fb/Link"));
//        System.out.println("success");
//        daoManager.createUser(
//                new User(GenerateUtil
//                        .getTokenByLogin("spectre"), "spectre", "1234", "Andrey",
//                        20, "sp@ya.ru", "blabla/myimage.jpg", "9833233668",
//                        "my/Vk/Link", "my/Fb/Link"));
//        System.out.println("success");
//        daoManager.createUser(
//                new User(GenerateUtil
//                        .getTokenByLogin("user"), "user", "1234", "Danil",
//                        20, "us@ya.ru", "blabla/myimage.jpg", "9833233668",
//                        "my/Vk/Link", "my/Fb/Link"));
////        System.out.println("success");

//        daoManager.updateUserDialog("goga", "Ru.Nov.Aura", "user", 1, DateUtil.getCurrentDateAsString());
//        System.out.println("Success");
//
//        DialogInfo dialogInfo = daoManager.selectUserDialogInfo("goga", "Ru.Nov.Aura", "user");
//        System.out.println(dialogInfo.getOwner() + " - " + dialogInfo.getInterlocutor() + " "
//                + dialogInfo.getCountMessage() + " / " + dialogInfo.getLastMessageTime());
//        User owner = daoManager.selectUserByToken("geomes_token_user");
//        User favorite = daoManager.selectUserInfoByLogin("goga");
//
//        daoManager.insertFavoriteUser(owner.getId(), favorite.getLogin());
//        Favorites result = daoManager.selectFavoriteUsers(owner.getLogin());
//
//        System.out.println(result.getOwner() + " [ " + result.getCount() + " ] :\n" + result.getFavoriteUsers());

//        daoManager.insertLikeByUserId(owner.getLogin(), liked.getId());
//        System.out.println("inserted");
//        Like resutl = daoManager.selectLikesByLogin(owner.getLogin());
//        System.out.println(resutl.getOwner() + " : " + resutl.getCountLikes());
//        System.out.println(Arrays.toString(resutl.getAdoringUsers().toArray()));
//
//        System.out.println(liked.getId());
//
//        Like result2 = daoManager.selectLikesByLogin(liked.getLogin());
//        System.out.println(result2.getOwner() + " : " + result2.getCountLikes());
//        System.out.println(Arrays.toString(result2.getAdoringUsers().toArray()));
//        daoManager.removeUserDialog("goga", "Ru.Nov.Aura", "user");
//        System.out.println("syccess");
//        DialogInfo info = daoManager.selectUserDialogInfo("goga", "Ru.Nov.Aura", "user");
//        System.out.println(info.getOwner() + " - " + info.getInterlocutor() + " "
//                + info.getCountMessage() + " / " + info.getLastMessageTime());

//        List<String> logins = new ArrayList<String>();
//        logins.add("test");
//        logins.add("user");
//        logins.add("spectre");
//        List<User> result = daoManager.selectListOfUserInfoByListOfLogins(logins);
//        if (result.isEmpty()) {
//            System.out.println("Not found");
//        } else {
//            for (User user : result)
//                System.out.println(user.getLogin() + " " + user.getPassword() + " : " + user.geteMail());
//        }
//        System.out.println(JsonUtil.userListToJson(result));
        //        DaoManager daoManager = new MysqlDaoManager();
//        daoManager.createUser(
//                new User(GenerateUtil
//                        .getTokenByLogin("goga"), "goga", "1234", "Goga",
//                        20, "goga@ya.ru", "blabla/myimage.jpg", "9833233668",
//                        "my/Vk/Link", "my/Fb/Link"));
//        System.out.println("success");
//        User user = daoManager.selectUserInfoByLogin("goga");
//        if (user == null) {
//            System.out.println("not found");
//            return;
//        }
//        System.out.println(user.getLogin() + " " + user.getPassword() + " : " + user.geteMail());
//        User user = daoManager.selectUserByToken("token1234");
//        System.out.println(user);
//
//        EntityManagerFactory factory = Persistence.createEntityManagerFactory("geomes_db");
//        System.out.println("create factory");
//        EntityManager entityManager =  factory.createEntityManager();
//        System.out.println("Create entity Manager");
//        entityManager.getTransaction().begin();


//        JpUserInfo userInfo = new JpUserInfo("Andrey", 20, "s@s.ru", null, null, null, null, null);
//        JpUser user = new JpUser("token", "user", "1234", userInfo);
//        userInfo.setJpUser(user);
//        System.out.println("pre persist");
//        entityManager.persist(user);
//        System.out.println("added");
//        JpUser user = entityManager.find(JpUser.class, 6);
//        System.out.println("get user: " + user.getLogin() + " - " + user.getUserInfo().geteMail());
//        System.out.println("get userINFO: " + user.getUserInfo().getJpUser().getToken());
////
//        entityManager.getTransaction().commit();
//        entityManager.close();
//        factory.close();
    }

//    private static boolean inRange(LocationCoords baseCoords, LocationCoords anotherCoords) {
//        double radius = Math.sqrt(
//                Math.pow(Math.abs(baseCoords.getLongitude() - anotherCoords.getLongitude()),2)
//                        + Math.pow(Math.abs(baseCoords.getLatitude() - anotherCoords.getLatitude()), 2));
//        System.out.println(radius);
//
//        return RADIUS_IN_RANGE.compareTo(radius) > 0;
//    }
}
