package ru.enschin.geomes.dao;

import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.DialogInfo;
import ru.enschin.geomes.model.Favorites;
import ru.enschin.geomes.model.Like;
import ru.enschin.geomes.model.User;

import java.util.List;

/**
 * Created by Andrew on 01.08.2016.
 */
public interface DaoManager {
    User selectUserByToken(String token) throws DaoSystemException, DaoBusinessException;
    User selectUserByLogin(String login, String password) throws DaoSystemException, DaoBusinessException;
    List<User> selectListOfUserInfoByListOfLogins(List<String> loginList) throws DaoSystemException;
    User selectUserInfoByLogin(String login) throws DaoSystemException, DaoBusinessException;
    DialogInfo selectUserDialogInfo(String login, String location, String interlocutor) throws DaoBusinessException, DaoSystemException;
    Like selectLikesByLogin(String login) throws DaoBusinessException, DaoSystemException;
    Favorites selectFavoriteUsers(String ownerLogin) throws DaoBusinessException, DaoSystemException;
    void createUser(User user) throws DaoBusinessException, DaoSystemException;
    void insertLikeByUserId(String ownerLogin, int likedId) throws DaoBusinessException, DaoSystemException;
    void insertFavoriteUser(int ownerId, String favoriteLogin) throws DaoBusinessException, DaoSystemException;
    void updateUserMood(String token, String mood) throws DaoSystemException, DaoBusinessException;
    void updateUserDialog(String login, String location, String interlocutor, int countMessage, String messageTime) throws DaoBusinessException, DaoSystemException;
    void removeUserDialog(String login, String location, String interlocutor) throws DaoBusinessException, DaoSystemException;

}
