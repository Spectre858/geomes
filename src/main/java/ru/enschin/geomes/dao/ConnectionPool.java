package ru.enschin.geomes.dao;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Andrew on 03.08.2016.
 */
@Deprecated
public class ConnectionPool {
    private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class.getName());

    private final String BASE_URL;
    private final String BASE_USER;
    private final String BASE_PASSWORD;


    private ConcurrentLinkedQueue<Connection> freePool = new ConcurrentLinkedQueue<Connection>();

    public ConnectionPool(int maxSize, String baseUrl, String baseUser, String basePassword) throws SQLException {
        BASE_URL = baseUrl;
        BASE_USER = baseUser;
        BASE_PASSWORD = basePassword;

        for (int i = 0; i < maxSize; ++i) {
            freePool.add(DriverManager.getConnection(baseUrl, baseUser, basePassword));
        }
        LOGGER.info("Created for " + maxSize + " connections");
    }

    public Connection getConnection() {
        try {
            Connection connection = freePool.remove();
            if (connection.isClosed()) {
                connection = DriverManager.getConnection(BASE_URL, BASE_USER, BASE_PASSWORD);
            }
            return connection;
        } catch (NoSuchElementException e) {
            LOGGER.warn("Connection pool is empty");
            return null;
        } catch (SQLException e) {
            LOGGER.error("Troubles with connections!!! ", e);
            return null;
        }
    }

    public void returnBackConnection(Connection connection) {
        try {
            if (connection.isClosed()) {
                connection = DriverManager.getConnection(BASE_URL, BASE_USER, BASE_PASSWORD);
            }
            connection.clearWarnings();
        } catch (SQLException e) {
            LOGGER.error("Troubles with connections!!! ", e);
        }
        if (!freePool.contains(connection)) {
            freePool.add(connection);
        }
    }

}
