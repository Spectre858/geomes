package ru.enschin.geomes.dao;

import org.apache.log4j.Logger;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoNoSuchEntityException;
import ru.enschin.geomes.model.Location;
import ru.enschin.geomes.model.LocationCoords;
import ru.enschin.geomes.model.WebSocketUserInfo;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Andrew on 29.08.2016.
 */
public class WebSocketUserInfoKeeper {
    private static final Logger LOGGER = Logger.getLogger(WebSocketUserInfoKeeper.class.getName());
    private static final String PLACE_BY_COORDS = "Place_by_coords";
    private final Map
            <String, Map
                    <String, Map
                            <String, Map
                                    <String, WebSocketUserInfo>>>> countrys = new ConcurrentHashMap<>();

    public WebSocketUserInfoKeeper() { }

    public void updateCoords(Location location, String userName, LocationCoords newCoords) throws DaoNoSuchEntityException {
        LOGGER.info("update coords");
        String placeName = location.getPlace();
        if (placeName == null) {
            placeName = PLACE_BY_COORDS;
        }
        WebSocketUserInfo result = null;
        try {
            result = countrys.get(location.getCountry())
                    .get(location.getCity())
                    .get(placeName)
                    .get(userName);

            result.getLocation().setCoords(newCoords);
        } catch (NullPointerException e) {
            throw new DaoNoSuchEntityException();
        }
    }

    public void addWebSocketUserInfo(WebSocketUserInfo info) throws DaoBusinessException {
        LOGGER.info("addWebSocketUserInfo");
        String countryName = info.getLocation().getCountry();
        Map<String, Map
                <String, Map
                        <String, WebSocketUserInfo>>> country = countrys.get(countryName);

        if (country == null) {
            country = createConcurrentTable(countryName, countrys);
//            country = new ConcurrentHashMap<>();
//            synchronized (countrys) {
//                if (!countrys.containsKey(countryName)) {
//                    countrys.put(countryName, country);
//                } else {
//                    country = countrys.get(countryName);
//                }
//            }
        }
        String cityName = info.getLocation().getCity();
        Map<String, Map
                <String, WebSocketUserInfo>> city = country.get(cityName);

        if (city == null) {
            city = createConcurrentTable(cityName, country);
//            city = new ConcurrentHashMap<>();
//            synchronized (country) {
//                if (!country.containsKey(cityName)) {
//                    country.put(cityName, city);
//                } else {
//                    city = country.get(cityName);
//                }
//            }
        }
        String placeName = info.getLocation().getPlace();
        if (placeName == null) {
            placeName = PLACE_BY_COORDS;
        }
        Map<String, WebSocketUserInfo> place = city.get(placeName);

        if (place == null) {
            place = createConcurrentTable(placeName, city);
//            place = new ConcurrentHashMap<>();
//            synchronized (city) {
//                if (!city.containsKey(placeName)) {
//                    city.put(placeName, place);
//                } else {
//                    place = city.get(placeName);
//                }
//            }
        }
        String userName = info.getUser().getLogin();
        WebSocketUserInfo userInfo = place.get(userName);

        if (userInfo == null) {
            userInfo = info;
            synchronized (place) {
                if (!place.containsKey(userName)) {
                    place.put(userName, userInfo);
                    return;
                }
            }
        }
        throw new DaoBusinessException("Entity already exist");
    }

    public void removeWebSocketUserInfo(Location location, String userName) {
        LOGGER.info("removeWebSocketUserInfo");
        Map<String, Map
                <String, Map
                        <String, WebSocketUserInfo>>> country = countrys.get(location.getCountry());
        if (country == null) {
            return;
        }
        Map<String, Map
                <String, WebSocketUserInfo>> city = country.get(location.getCity());
        if (city == null) {
            return;
        }
        String placeName = location.getPlace() == null ? PLACE_BY_COORDS : location.getPlace();
        Map<String, WebSocketUserInfo> place = city.get(placeName);
        if (place == null) {
            return;
        }
        WebSocketUserInfo userInfo = place.get(userName);
        if (userInfo != null) {
            place.remove(userName);
        }

    }

    public WebSocketUserInfo getWebSocketUserInfo(Location location, String userName) throws DaoNoSuchEntityException {
        LOGGER.info("getWebSocketUserInfo");
        String placeName = location.getPlace();
        if (placeName == null) {
            placeName = PLACE_BY_COORDS;
        }
        WebSocketUserInfo result = null;
        try {
            result = countrys.get(location.getCountry())
                    .get(location.getCity())
                    .get(placeName)
                    .get(userName);
        } catch (NullPointerException e) {
            throw new DaoNoSuchEntityException();
        }
        return result;
    }

    public List<WebSocketUserInfo> getListOfWebSocketUserInfoAtPlace(Location location) {
        LOGGER.info("getListOfWebSocketUserInfoAtPlace");
        List<WebSocketUserInfo> result = new ArrayList<>();

        try {
            String placeName = location.getPlace();
            if (placeName == null) {
                placeName = PLACE_BY_COORDS;
            }
            Map<String, WebSocketUserInfo> users = countrys.get(location.getCountry())
                    .get(location.getCity())
                    .get(placeName);
            Collection<WebSocketUserInfo> collection = users.values();
            result.addAll(collection);
        } catch (NullPointerException e) {
//            Ignore
        }
        return result;
    }

    public List<WebSocketUserInfo> getListOfWebSocketUserInfoInCity(Location location) {
        LOGGER.info("getListOfWebSocketUserInfoInCity");
        List<WebSocketUserInfo> result = new ArrayList<>();

        try {
            Map<String, Map<String, WebSocketUserInfo>> city = countrys
                    .get(location.getCountry())
                    .get(location.getCity());
            for (Map<String, WebSocketUserInfo> places : city.values()) {
                result.addAll(places.values());
            }
        } catch (NullPointerException e) {
//            Ignore
        }
        return result;
    }

    public boolean contains(Location location, String userName) {
        LOGGER.info("is contains");
        boolean result = false;
        try {
            String placeName = location.getPlace() == null ? PLACE_BY_COORDS : location.getPlace();
            result = countrys.get(location.getCountry())
                    .get(location.getCity())
                    .get(placeName)
                    .containsKey(userName);
        } catch (NullPointerException e) {
//            Ignore
        }
        return result;
    }



    private Map createConcurrentTable(String name, Map parentTable) {
        Map table = new ConcurrentHashMap<>();
        synchronized (parentTable) {
            if (!parentTable.containsKey(name)) {
                parentTable.put(name, table);
            } else {
                table = (Map) parentTable.get(name);
            }
        }
        return table;
    }
}
