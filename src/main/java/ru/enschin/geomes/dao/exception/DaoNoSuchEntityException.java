package ru.enschin.geomes.dao.exception;

/**
 * Created by Andrew on 23.01.2016.
 */
public class DaoNoSuchEntityException extends DaoBusinessException {
    public DaoNoSuchEntityException() {
        super();
    }

    public DaoNoSuchEntityException(String message) {
        super(message);
    }

    public DaoNoSuchEntityException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
