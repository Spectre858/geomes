package ru.enschin.geomes.dao.exception;

/**
 * Created by Andrew on 23.01.2016.
 */
public class DaoBaseException extends Exception {
    public DaoBaseException() {
        super();
    }
    public DaoBaseException(String message) {
        super(message);
    }
    public DaoBaseException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
