package ru.enschin.geomes.dao.exception;

/**
 * Created by Andrew on 23.01.2016.
 */
public class DaoBusinessException extends DaoBaseException {
    public DaoBusinessException() {
        super();
    }

    public DaoBusinessException(String message) {
        super(message);
    }

    public DaoBusinessException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
