package ru.enschin.geomes.dao.exception;

/**
 * Created by Andrew on 23.01.2016.
 */
public class DaoSystemException extends DaoBaseException {
    public DaoSystemException() {
        super();
    }
    public DaoSystemException(String message) {
        super(message);
    }
    public DaoSystemException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
