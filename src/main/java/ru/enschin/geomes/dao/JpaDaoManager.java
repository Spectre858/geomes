package ru.enschin.geomes.dao;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoNoSuchEntityException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.DialogInfo;
import ru.enschin.geomes.model.Favorites;
import ru.enschin.geomes.model.Like;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.model.jpentity.*;
import ru.enschin.geomes.util.DateUtil;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 13.08.2016.
 */
public class JpaDaoManager implements DaoManager {
    private static final Logger LOGGER = Logger.getLogger(JpaDaoManager.class.getName());
    private EntityManagerFactory entityManagerFactory;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public JpaDaoManager(EntityManagerFactory entityManagerFactory, BCryptPasswordEncoder passwordEncoder) {
        this.entityManagerFactory = entityManagerFactory;
        this.passwordEncoder = passwordEncoder;
    }

    public User selectUserByToken(String token) throws DaoSystemException, DaoBusinessException {
        LOGGER.info("select user by token");
        EntityManager em = entityManagerFactory.createEntityManager();
        User resultUser = null;

        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);
            criteriaQuery.select(us)
                    .where(criteriaBuilder.equal(us.get("token"), token));
            Query query = em.createQuery(criteriaQuery);
            JpUser jpUser = (JpUser) query.getSingleResult();
            resultUser = jpUser.toUser();
        } catch (NoResultException e) {
            LOGGER.info("No result for such token");
            throw new DaoNoSuchEntityException("No such entity", e);
        } catch (Exception e) {
            LOGGER.warn("Some error then execute query in select user by token", e);
            throw new DaoSystemException("Some error then execute query in select user by token", e);
        } finally {
            em.close();
        }
        return resultUser;
    }

    public User selectUserByLogin(String login, String password) throws DaoSystemException, DaoBusinessException {
        LOGGER.info("select user by login");
//        password = passwordEncoder.encode(password);
        EntityManager em = entityManagerFactory.createEntityManager();
        User resultUser = null;

        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);
            criteriaQuery.select(us)
                    .where(criteriaBuilder.equal(us.get("login"), login));
//                    .where(criteriaBuilder.and(
//                            criteriaBuilder.equal(us.get("login"), login),
//                            criteriaBuilder.equal(us.get("password"), password)
//                    ));
            Query query = em.createQuery(criteriaQuery);
            JpUser jpUser = (JpUser) query.getSingleResult();
            if (passwordEncoder.matches(password, jpUser.getPassword())) {
                resultUser = jpUser.toUser();
            }
        } catch (NoResultException e) {
            LOGGER.info("No result for such login");
            throw new DaoBusinessException("No such entity", e);
        } catch (Exception e) {
            LOGGER.warn("Some error then execute query in select user by login", e);
            Throwable []couses = e.getSuppressed();

            /*Trying to connect to database again if Exception is CommunicationException*/
            boolean f = false;
            for (Throwable thr : couses) {
                if (thr instanceof CommunicationsException) {
                    f = true;
                    break;
                }
            }
            if (f) {
                LOGGER.warn(CommunicationsException.class.getName() + "in selectUserByLogin");
                selectUserByLogin(login, password);
            } else {
                throw new DaoSystemException("Some error then execute query in select user by login", e);
            }
            /*todo: Remove block above if don't work and incomment throw below*/
            /*todo: do it for all function in Manager if it work*/
            /*throw new DaoSystemException("Some error then execute query in select user by login", e);*/
        } finally {
            em.close();
        }
        return resultUser;
    }

    public List<User> selectListOfUserInfoByListOfLogins(List<String> loginList) throws DaoSystemException {
        LOGGER.info("select list of user info by list of logins");
        List<User> resultList = new ArrayList<User>();
        if (loginList.isEmpty()) {
            return resultList;
        }

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);
            Predicate[] predicates = new Predicate[loginList.size()];

            for (int i = 0; i < loginList.size(); ++i) {
                predicates[i] = criteriaBuilder.equal(us.get("login"), loginList.get(i));
            }
            criteriaQuery.select(us)
                    .where(criteriaBuilder.or(predicates));

            Query query = em.createQuery(criteriaQuery);
            List<JpUser> userList = query.getResultList();

            for (JpUser ju : userList) {
                ju.setPassword(null);
                ju.setToken(null);
                resultList.add(ju.toUser());
            }

        } catch (Exception e) {
            LOGGER.warn("Some error then execute query in select user info by login", e);
            throw new DaoSystemException("Some error then execute query in select user info by login", e);
        } finally {
            em.close();
        }
        return resultList;
    }

    public User selectUserInfoByLogin(String login) throws DaoSystemException, DaoBusinessException {
        LOGGER.info("select user info by login");
        EntityManager em = entityManagerFactory.createEntityManager();
        User resultUser = null;

        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);
            Join usInfo = us.join("userInfo");
            criteriaQuery.select(usInfo)
                    .where(criteriaBuilder.equal(us.get("login"), login));
            Query query = em.createQuery(criteriaQuery);
            JpUserInfo jpUserInfo = (JpUserInfo) query.getSingleResult();
            resultUser = jpUserInfo.toUser();
            resultUser.setLogin(login);
        } catch (NoResultException e) {
            LOGGER.info("No result for such login");
            throw new DaoBusinessException("No such entity", e);
        } catch (Exception e) {
            LOGGER.warn("Some error then execute query in select user info by login", e);
            throw new DaoSystemException("Some error then execute query in select user info by login", e);
        } finally {
            em.close();
        }
        return resultUser;
    }

    public DialogInfo selectUserDialogInfo(String login, String location, String interlocutor) throws DaoBusinessException, DaoSystemException {
        LOGGER.info("select user dialog info");
        DialogInfo result = null;
        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);

            criteriaQuery.select(us)
                    .where(criteriaBuilder.equal(us.get("login"), login));
            Query query = em.createQuery(criteriaQuery);
            JpUser user = (JpUser) query.getSingleResult();
            List<JpDialogList> dialogList = user.getSystemInfo().getDialogList();
            if (dialogList != null) {
                for (JpDialogList dialog : dialogList) {
                    if ((dialog.getLocation().compareTo(location) == 0)
                            && (dialog.getInterlocutor().compareTo(interlocutor) == 0)) {
                        result = dialog.toDialogInfo();
                        break;
                    }
                }
            }

        } catch (NoResultException e) {
            LOGGER.info("No result for such login");
            throw new DaoBusinessException("No such entity", e);
        } catch (Exception e) {
            LOGGER.warn("Some error then execute query on select dialog info", e);
            em.getTransaction().rollback();
            throw new DaoSystemException("Some error then execute query on select dialog info", e);
        } finally {
            em.close();
        }
        return result;
    }

    public Like selectLikesByLogin(String login) throws DaoBusinessException, DaoSystemException {
        LOGGER.info("Select like by user id");
        EntityManager em = entityManagerFactory.createEntityManager();
        Like result = null;

        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);

            criteriaQuery.select(us)
                    .where(criteriaBuilder.equal(us.get("login"), login));
            Query query = em.createQuery(criteriaQuery);
            JpUser user = (JpUser) query.getSingleResult();

            JpLike like = user.getSystemInfo().getLikes();
            if (like != null) {
                result = like.toLike();
            }
        } catch (NoResultException e) {
            LOGGER.info("No result for such id");
            throw new DaoBusinessException("No such entity", e);
        } catch (Exception e) {
            LOGGER.warn("Some error then execute query on select like", e);
            throw new DaoSystemException("Some error then execute query on select like", e);
        } finally {
            em.close();
        }
        return result;
    }

    @Override
    public Favorites selectFavoriteUsers(String ownerLogin) throws DaoBusinessException, DaoSystemException {
        LOGGER.info("Select favorite users");
        EntityManager em = entityManagerFactory.createEntityManager();
        Favorites result = null;

        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);

            criteriaQuery.select(us)
                    .where(criteriaBuilder.equal(us.get("login"), ownerLogin));
            Query query = em.createQuery(criteriaQuery);
            JpUser user = (JpUser) query.getSingleResult();

            JpFavoriteUsers favoriteUsers = user.getSystemInfo().getFavoriteUsers();
            if (favoriteUsers != null) {
                result = favoriteUsers.toFavorites();
            }
        } catch (NoResultException e) {
            LOGGER.info("No result for such id");
            throw new DaoBusinessException("No such entity", e);
        } catch (Exception e) {
            LOGGER.warn("Some error then execute query on select like", e);
            throw new DaoSystemException("Some error then execute query on select like", e);
        } finally {
            em.close();
        }
        return result;
    }

    public void createUser(User user) throws DaoBusinessException, DaoSystemException {
        LOGGER.info("Creating new user");
        EntityManager em = entityManagerFactory.createEntityManager();
        JpUser jpUser = new JpUser();
        jpUser.setAllFields(user, DateUtil.getCurrentDateAsString(), null, null, null);
        jpUser.setPassword(passwordEncoder.encode(user.getPassword()));

        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);
//        criteriaQuery.select(us.get("token"), us.get("login"), us.get("userInfo").get("eMail"));
            criteriaQuery.multiselect(
                    us.get("token"), us.get("login"), us.get("userInfo").get("eMail")
            );
            criteriaQuery.where(
                    criteriaBuilder.or(
                            criteriaBuilder.equal(us.get("token"), jpUser.getToken()),
                            criteriaBuilder.equal(us.get("login"), jpUser.getLogin()),
                            criteriaBuilder.equal(us.get("userInfo").get("eMail"), jpUser.getUserInfo().geteMail())
                    )
            );
            Query query = em.createQuery(criteriaQuery);
            List result = query.getResultList();
            if (result != null) {
                if (!result.isEmpty()) {
                    throw new DaoBusinessException("User with such unique data already exist");
                }
            }
            em.getTransaction().begin();
            em.persist(jpUser);
            em.getTransaction().commit();
        } catch (DaoBusinessException e) {
            LOGGER.info("User with such unique data already exist");
            throw e;
        } catch (Exception e) {
            LOGGER.warn("Some error then execute query on create user", e);
            em.getTransaction().rollback();
            throw new DaoSystemException("Some error then execute query on create user", e);
        } finally {
            em.close();
        }
    }

    public void insertLikeByUserId(String ownerLogin, int likedId) throws DaoNoSuchEntityException, DaoBusinessException, DaoSystemException {
        LOGGER.info("Inserting like");
        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);
            criteriaQuery.select(us)
                    .where(criteriaBuilder.equal(us.get("login"), ownerLogin));
            Query query = em.createQuery(criteriaQuery);

            JpUser liked = em.find(JpUser.class, likedId);
            JpUser owner = (JpUser) query.getSingleResult();


            JpLike like = owner.getSystemInfo().getLikes();
            if (like == null) {
                like = new JpLike(0, owner.getSystemInfo(), new ArrayList<JpUser>());
                owner.getSystemInfo().setLikes(like);
            }
            for (JpUser tmp : like.getUserList()) {
                if (tmp.getSystemInfo().getUser().getLogin().compareTo(liked.getLogin()) == 0) {
                    throw new DaoBusinessException("like by this user already stated");
                }
            }
            like.setCount(like.getCount() + 1);
            like.getUserList().add(liked);

            em.merge(owner);
            em.getTransaction().commit();
        } catch (EntityNotFoundException | NoResultException | NullPointerException e) {
            LOGGER.info("No result for such id or login");
            em.getTransaction().rollback();
            throw new DaoNoSuchEntityException("No such entity", e);
        } catch (DaoBusinessException e) {
            LOGGER.info(e.getMessage());
            em.getTransaction().rollback();
            throw e;
        } catch (Exception e) {
            LOGGER.warn("Some error then execute query on insert like", e);
            em.getTransaction().rollback();
            throw new DaoSystemException("Some error then execute query on insert like", e);
        } finally {
            em.close();
        }
    }

    @Override
    public void insertFavoriteUser(int ownerId, String favoriteLogin) throws DaoBusinessException, DaoSystemException {
        LOGGER.info("Inserting favorite user");
        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);
            criteriaQuery.select(us)
                    .where(criteriaBuilder.equal(us.get("login"), favoriteLogin));
            Query query = em.createQuery(criteriaQuery);

            JpUser owner = em.find(JpUser.class, ownerId);
            JpUser favoriteUser = (JpUser) query.getSingleResult();


            JpFavoriteUsers favorites = owner.getSystemInfo().getFavoriteUsers();
            if (favorites == null) {
                favorites = new JpFavoriteUsers(0, owner.getSystemInfo(), new ArrayList<JpUser>());
                owner.getSystemInfo().setFavoriteUsers(favorites);
            }
            for (JpUser tmp : favorites.getUserList()) {
                if (tmp.getSystemInfo().getUser().getLogin().compareTo(favoriteUser.getLogin()) == 0) {
                    throw new DaoBusinessException("This user already elected");
                }
            }
            favorites.setCount(favorites.getCount() + 1);
            favorites.getUserList().add(favoriteUser);

            em.merge(owner);
            em.getTransaction().commit();
        } catch (EntityNotFoundException | NoResultException | NullPointerException e) {
            LOGGER.info("No result for such id or login");
            em.getTransaction().rollback();
            throw new DaoBusinessException("No such entity", e);
        } catch (DaoBusinessException e) {
            LOGGER.info(e.getMessage());
            em.getTransaction().rollback();
            throw e;
        } catch (Exception e) {
            LOGGER.warn("Some error then execute query on insert favorite user", e);
            em.getTransaction().rollback();
            throw new DaoSystemException("Some error then execute query on insert favorite user", e);
        } finally {
            em.close();
        }
    }

    @Override
    public void updateUserMood(String token, String mood) throws DaoSystemException, DaoBusinessException {
        LOGGER.info("updateUserMood");
        EntityManager em = entityManagerFactory.createEntityManager();
        User resultUser = null;

        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);
            criteriaQuery.select(us)
                    .where(criteriaBuilder.equal(us.get("token"), token));
            Query query = em.createQuery(criteriaQuery);
            JpUser jpUser = (JpUser) query.getSingleResult();
            jpUser.getUserInfo().setMood(mood);

            em.getTransaction().begin();
            em.merge(jpUser);
            em.getTransaction().commit();
        } catch (NoResultException e) {
            LOGGER.info("No result for such token");
            throw new DaoBusinessException("No such entity", e);
        } catch (Exception e) {
            LOGGER.warn("Some error then execute query in select user by token", e);
            em.getTransaction().rollback();
            throw new DaoSystemException("Some error then execute query in select user by token", e);
        } finally {
            em.close();
        }
    }

    public void updateUserDialog(String login, String location, String interlocutor, int countMessage, String messageTime) throws DaoBusinessException, DaoSystemException {
        LOGGER.info("Updating user dialog");
        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);
            criteriaQuery.select(us)
                    .where(criteriaBuilder.equal(us.get("login"), login));
            Query query = em.createQuery(criteriaQuery);
            JpUser user = (JpUser) query.getSingleResult();
            if (user.getSystemInfo() == null) {
                user.setSystemInfo(new JpUserSystemInfo(DateUtil.getCurrentDateAsString(), null, user, null, null));
            }

            List<JpDialogList> dialogList = user.getSystemInfo().getDialogList();
            if (dialogList == null) {
                dialogList = new ArrayList<JpDialogList>();
            }
            JpDialogList dialog = null;

            for (JpDialogList dia : dialogList) {
                if ((dia.getLocation().compareTo(location) == 0)
                        && (dia.getInterlocutor().compareTo(interlocutor) == 0)) {
                    dialog = dia;
                }
            }
            if (dialog == null) {
                dialogList.add(new JpDialogList(location, interlocutor, messageTime, countMessage, user.getSystemInfo()));
            } else {
                dialog.setLastMessageTime(messageTime);
                dialog.setCountMessage(countMessage);
            }

            em.getTransaction().begin();
            em.merge(user);
            em.getTransaction().commit();
        } catch (NoResultException e) {
            LOGGER.info("No result for such login");
            throw new DaoBusinessException("No such entity", e);
        } catch (Exception e) {
            LOGGER.warn("Some error then execute query on update user dialog", e);
            em.getTransaction().rollback();
            throw new DaoSystemException("Some error then execute query on update user dialog", e);
        } finally {
            em.close();
        }
    }

    public void removeUserDialog(String login, String location, String interlocutor) throws DaoBusinessException, DaoSystemException {
        LOGGER.info("Remove user dialog");
        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
            Root us = criteriaQuery.from(JpUser.class);
            criteriaQuery.select(us)
                    .where(criteriaBuilder.equal(us.get("login"), login));
            Query query = em.createQuery(criteriaQuery);
            JpUser user = (JpUser) query.getSingleResult();

            List<JpDialogList> dialogList = user.getSystemInfo().getDialogList();
            JpDialogList resultDialog = null;

            for (JpDialogList dialog : dialogList) {
                if ((dialog.getLocation().compareTo(location) == 0)
                        && (dialog.getInterlocutor().compareTo(interlocutor) == 0)) {
                    resultDialog = dialog;
                    dialogList.remove(dialog);
                    break;
                }
            }

            if (resultDialog != null) {
                em.getTransaction().begin();
                em.remove(resultDialog);
                em.getTransaction().commit();
            }
        } catch (NoResultException e) {
            LOGGER.info("No result for such login");
            throw new DaoBusinessException("No such entity", e);
        } catch (Exception e) {
            LOGGER.warn("Some error then execute query on remove user dialog", e);
            em.getTransaction().rollback();
            throw new DaoSystemException("Some error then execute query on remove user dialog", e);
        } finally {
            em.close();
        }
    }
}
