package ru.enschin.geomes.dao;

import org.apache.log4j.Logger;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.DialogInfo;
import ru.enschin.geomes.model.Favorites;
import ru.enschin.geomes.model.Like;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.util.ValidateUtil;

import java.sql.*;
import java.util.List;

/**
 * Created by Andrew on 01.08.2016.
 */
@Deprecated
public class MysqlDaoManager implements DaoManager {
    protected static final Logger LOGGER = Logger.getLogger(MysqlDaoManager.class.getName());
    private static final String BASE_URL = "jdbc:mysql://localhost:3306/geomes_db";
    private static final String BASE_USER_NAME = "root";
    private static final String BASE_USER_PASSWORD = "eab0109371443";

    private static final int MAX_SIZE_CONNECTION = 5;

    private ConnectionPool connections = null;

    public MysqlDaoManager() throws DaoSystemException {
        try {
            LOGGER.info("initialize the database");
            Class.forName("com.mysql.jdbc.Driver");
            connections = new ConnectionPool(MAX_SIZE_CONNECTION, BASE_URL, BASE_USER_NAME, BASE_USER_PASSWORD);
            checkTablesExistence();
        } catch (SQLException e) {
            LOGGER.fatal("couldn't connect to database",e);
            throw new DaoSystemException("couldn't connect to database", e);
        } catch (ClassNotFoundException e) {
            LOGGER.fatal("couldn't find the JDBC Driver -> com.mysql.jdbc.Driver", e);
            throw new DaoSystemException("couldn't find the JDBC Driver -> com.mysql.jdbc.Driver", e);
        }
    }

    public User selectUserByToken(String token) throws DaoSystemException {
        LOGGER.info("select user by token: " + token);
        User result = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = connections.getConnection();
            connection.setAutoCommit(true);
            statement = connection.createStatement();

            resultSet = statement.executeQuery(String.format("SELECT " +
                    "users.id, last_token, login, password, full_name, age, e_mail, profile_picture_link, telephone, vk_link, fb_link " +
                    "FROM users JOIN user_info ON users.id=user_info.id " +
                    "WHERE users.last_token='%s'", token));

            if (resultSet.first()) {
                result = new User(resultSet.getInt("users.id"), resultSet.getString("last_token"),
                        resultSet.getString("login"), resultSet.getString("password"), null, resultSet.getString("full_name"),
                        resultSet.getInt("age"), resultSet.getString("e_mail"), resultSet.getString("profile_picture_link"),
                        resultSet.getString("telephone"), resultSet.getString("vk_link"), resultSet.getString("fb_link"));
            }
        } catch (SQLException e) {
            LOGGER.warn("can't to select from tables a user with token: " + token, e);
            throw new DaoSystemException("can't to select from tables a user with token: " + token, e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException e) {
//                Ignore
            } finally {
                connections.returnBackConnection(connection);
            }
        }
        return result;
    }

    public synchronized User selectUserByLogin(String login, String password) throws DaoSystemException {
        LOGGER.info("select user by login: " + login + " password: " + password);
        User result = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = connections.getConnection();
            connection.setAutoCommit(true);
            statement = connection.createStatement();

            resultSet = statement.executeQuery(String.format("SELECT " +
                    "users.id, last_token, login, password, full_name, age, e_mail, profile_picture_link, telephone, vk_link, fb_link " +
                    "FROM users JOIN user_info ON users.id=user_info.id " +
                    "WHERE users.login='%s' AND users.password='%s'", login, password));

            if (resultSet.first()) {
                result = new User(resultSet.getInt("users.id"), resultSet.getString("last_token"),
                        resultSet.getString("login"), resultSet.getString("password"), null, resultSet.getString("full_name"),
                        resultSet.getInt("age"), resultSet.getString("e_mail"), resultSet.getString("profile_picture_link"),
                        resultSet.getString("telephone"), resultSet.getString("vk_link"), resultSet.getString("fb_link"));
            }
        } catch (SQLException e) {
            LOGGER.warn("can't to select from tables a user with login: " + login + " and password: " + password, e);
            throw new DaoSystemException("can't to select from tables a user with login: " + login + " and password: " + password, e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException e) {
//                Ignore
            } finally {
                connections.returnBackConnection(connection);
            }
        }
        return result;
    }

    public List<User> selectListOfUserInfoByListOfLogins(List<String> loginList) {
        return null;
    }

    public User selectUserInfoByLogin(String login) throws DaoSystemException {
        LOGGER.info("select user info by login: " + login);

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        User user = null;

        try {
            connection = connections.getConnection();
            connection.setAutoCommit(true);
            statement = connection.createStatement();
            resultSet = statement.
                    executeQuery(String.format("SELECT id FROM users WHERE login='%s'", login));
            if (resultSet.first()) {
                int id = resultSet.getInt("id");
                resultSet.close();
                resultSet = statement.
                        executeQuery(String.format("SELECT " +
                                "full_name, age, e_mail, profile_picture_link, " +
                                "telephone, vk_link, fb_link " +
                                "FROM user_info WHERE id='%s'", id));
                resultSet.first();
                user = new User(0, null, login, null, null, resultSet.getString("full_name"),
                        resultSet.getInt("age"), resultSet.getString("e_mail"),
                        resultSet.getString("profile_picture_link"),
                        resultSet.getString("telephone"), resultSet.getString("vk_link"),
                        resultSet.getString("fb_link"));
            }
        } catch (SQLException e) {
            LOGGER.warn("some error when select userInfo by login", e);
            throw new DaoSystemException("some error when select userInfo by login", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException e) {
//                Ignore
            } finally {
                connections.returnBackConnection(connection);
            }
        }

        return user;
    }

    public DialogInfo selectUserDialogInfo(String login, String location, String interlocutor) {
        return null;
    }

    public Like selectLikesByLogin(String login) throws DaoBusinessException, DaoSystemException {
        return null;
    }

    @Override
    public Favorites selectFavoriteUsers(String ownerLogin) {
        return null;
    }

    /**
     * Creating the full user info in database by @param user
     * @throws DaoBusinessException in case of error
     */

    public synchronized void createUser(User user) throws DaoBusinessException {
        LOGGER.info("create user: " + user);

        if (!ValidateUtil.validateOfUser(user)) {
            throw new DaoBusinessException("Invalid user value: " + user);
        }

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = connections.getConnection();
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            statement.executeUpdate(String.format("INSERT INTO users (last_token, login, password) VALUES " +
                    "(\'%s\', \'%s\', \'%s\')", user.getToken(), user.getLogin(), user.getPassword()));

            resultSet = statement.executeQuery(String.format("SELECT id FROM users WHERE last_token=\'%s\'", user.getToken()));
            resultSet.first();
            statement.executeUpdate(String.format("INSERT INTO user_info (" +
                    "id, full_name, age, e_mail, profile_picture_link, telephone, vk_link, fb_link" +
                    ") VALUES " +
                    "(\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\')",
                    resultSet.getInt("id"), user.getFullName(), user.getAge(), user.geteMail(),
                    user.getProfilePictureLink(), user.getTelephoneNumber(), user.getVkLink(), user.getFbLink()));
            connection.commit();
        } catch (SQLException e) {
            LOGGER.warn("some error in time creating of new user" + user, e);
            try {
                connection.rollback();
            } catch (SQLException e1) {
//                Ignore
            } finally {
                throw new DaoBusinessException("some error in time creating of new user" + user, e);
            }
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException e) {
//                Ignore
            } finally {
                connections.returnBackConnection(connection);
            }
        }
    }

    public void insertLikeByUserId(String ownerLogin, int likedId) throws DaoBusinessException, DaoSystemException {

    }

    @Override
    public void insertFavoriteUser(int ownerId, String favoriteLogin) {

    }

    @Override
    public void updateUserMood(String token, String mood) {

    }

    public void updateUserDialog(String login, String location, String interlocutor, int countMessage, String messageTime) {

    }

    public void removeUserDialog(String login, String location, String interlocutor) {

    }


    /**
     * Check the existence tables of database and create they
     * if they not exists.
     *
     * @throws DaoSystemException if do not able to create a table
     */

    private void checkTablesExistence() throws DaoSystemException {
        LOGGER.info("check tables on existence");
        Connection connection = null;
        Statement statement = null;
        try {
            connection = connections.getConnection();
            connection.setAutoCommit(false);
            statement = connection.createStatement();
//            statement.executeUpdate("DROP TABLE user_info");
//            statement.executeUpdate("DROP TABLE users");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS users (" +
                    "id INTEGER UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                    "last_token VARCHAR(255) NOT NULL UNIQUE," +
                    "login VARCHAR(255) NOT NULL UNIQUE ," +
                    "password VARCHAR(255) NOT NULL" +
                    ") ENGINE = InnoDB DEFAULT CHARSET = utf8;");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS user_info(" +
                    "id INTEGER UNSIGNED NOT NULL PRIMARY KEY," +
                    "full_name VARCHAR(255) NOT NULL," +
                    "age INTEGER NOT NULL," +
                    "e_mail VARCHAR(255) NOT NULL UNIQUE ," +
                    "profile_picture_link VARCHAR(255)," +
                    "telephone VARCHAR(12)," +
                    "vk_link VARCHAR(255)," +
                    "fb_link VARCHAR(255)," +
                    "FOREIGN KEY(id) REFERENCES users(id)" +
                    ") ENGINE = InnoDB DEFAULT CHARSET = utf8;");

            connection.commit();
        } catch (SQLException e) {
            LOGGER.fatal("Can't to create a tables of database!", e);
            try {
                connection.rollback();
            } catch (SQLException e1) {
//                Ignore
            }
            throw new DaoSystemException("Can't to create a tables of database!", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
//               Ignore
            } finally {
                connections.returnBackConnection(connection);
            }
        }
    }

}
