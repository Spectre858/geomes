package ru.enschin.geomes.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by Andrew on 01.09.2016.
 */
public class TimeGaugeOfRequestsFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(TimeGaugeOfRequestsFilter.class.getName());
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        double startTime = System.currentTimeMillis();
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        String requestUrl = request.getRequestURL().toString();

        String remoteAddr = request.getRemoteAddr();

        filterChain.doFilter(servletRequest, servletResponse);

        double endTime = System.currentTimeMillis();
        double resultTime = endTime - startTime;
        resultTime /= 1000;
        LOGGER.debug(String.format("[%.6f sec] %s -> %s", resultTime, remoteAddr, requestUrl));
    }

    @Override
    public void destroy() {

    }
}
