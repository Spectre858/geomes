package ru.enschin.geomes.inject;

import java.lang.annotation.*;

/**
 * Created by Andrew on 19.06.2016.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {
    public String value();
}
