package ru.enschin.geomes.controller;

import org.apache.log4j.Logger;
import ru.enschin.geomes.model.Error;
import ru.enschin.geomes.util.JsonUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Andrew on 06.08.2016.
 */
public class ServletErrorController extends ServletBaseController {
    private static final Logger LOGGER = Logger.getLogger(ServletErrorController.class.getName());

    private static final String PARAM_ERROR_TYPE = "error_type";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter(PARAM_ERROR_TYPE);

        resp.setContentType("json/text");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        if (type == null) {
            LOGGER.info("Invalid request parameters");
            out.write(JsonUtil.getErrorJson(Error.Type.INVALID_REQUEST_PARAMETERS));
            return;
        }

        if (Error.Type.INVALID_REQUEST_PARAMETERS.name().compareTo(type) == 0) {
            LOGGER.info(type);
            out.write(JsonUtil.getErrorJson(Error.Type.INVALID_REQUEST_PARAMETERS));

        } else if (Error.Type.INVALID_AUTHORIZATION.name().compareTo(type) == 0) {
            LOGGER.info(type);
            out.write(JsonUtil.getErrorJson(Error.Type.INVALID_AUTHORIZATION));

        } else if (Error.Type.NO_SUCH_ENTITY.name().compareTo(type) == 0) {
            LOGGER.info(type);
            out.write(JsonUtil.getErrorJson(Error.Type.NO_SUCH_ENTITY));

        } else if (Error.Type.DISCREPANCY_OF_LOCATION.name().compareTo(type) == 0) {
            LOGGER.info(type);
            out.write(JsonUtil.getErrorJson(Error.Type.DISCREPANCY_OF_LOCATION));

        } else if (Error.Type.SYSTEM_ERROR.name().compareTo(type) == 0) {
            LOGGER.info(type);
            out.write(JsonUtil.getErrorJson(Error.Type.SYSTEM_ERROR));

        } else  if (Error.Type.INVALID_EMAIL.name().compareTo(type) == 0) {
            LOGGER.info(type);
            out.write(JsonUtil.getErrorJson(Error.Type.INVALID_EMAIL));

        } else  if (Error.Type.INVALID_REQUEST_TYPE.name().compareTo(type) == 0) {
            LOGGER.info(type);
            out.write(JsonUtil.getErrorJson(Error.Type.INVALID_REQUEST_TYPE));

        } else  if (Error.Type.ENTITY_ALREADY_EXIST.name().compareTo(type) == 0) {
            LOGGER.info(type);
            out.write(JsonUtil.getErrorJson(Error.Type.ENTITY_ALREADY_EXIST));

        } else {
            LOGGER.info("Invalid request parameters");
            out.write(JsonUtil.getErrorJson(Error.Type.INVALID_REQUEST_PARAMETERS));

        }
    }
}
