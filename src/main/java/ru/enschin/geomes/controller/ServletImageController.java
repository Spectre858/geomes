package ru.enschin.geomes.controller;

import org.apache.log4j.Logger;
import ru.enschin.geomes.model.Error;
import ru.enschin.geomes.util.ValidateUtil;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Andrew on 09.08.2016.
 */
public class ServletImageController extends ServletBaseController {
    private static final Logger LOGGER = Logger.getLogger(ServletImageController.class.getName());
    private static final String PHOTO_DIR_LOCATION = "photoDirLocation";
    private static final String PREV_DIR_PREFIX = "image";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("doGet()");
        String path = getServletContext().getInitParameter(PHOTO_DIR_LOCATION);
        String fineName = getFileName(req.getRequestURL().toString());
        try {
            if (!ValidateUtil.fullValidation(fineName)) {
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                return;
            }
            File file = new File(path + fineName);
            if (!file.exists()) {
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY);
                return;
            }
            resp.setContentType("image");
            resp.setCharacterEncoding("UTF-8");
            BufferedImage image = ImageIO.read(file);
            ImageIO.write(image, "jpeg", resp.getOutputStream());
        } catch (IOException e) {
            LOGGER.warn("Some IO error", e);
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.SYSTEM_ERROR);
        }
    }

    private String getFileName(String url) {
        String[] spl = url.split("/");

        for (int i = 0; i < spl.length; ++i) {
            if (PREV_DIR_PREFIX.compareTo(spl[i]) == 0) {
                if (i + 1 < spl.length)
                    return spl[i + 1];
            }
        }
        return null;
    }


}
