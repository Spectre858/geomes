package ru.enschin.geomes.controller;

import org.apache.log4j.Logger;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.*;
import ru.enschin.geomes.model.Error;
import ru.enschin.geomes.util.SecureUtil;
import ru.enschin.geomes.util.JsonUtil;
import ru.enschin.geomes.util.ValidateUtil;
import ru.enschin.geomes.util.WebSocketUserInfoHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 06.08.2016.
 */
public class ServletPeopleNearbyController extends ServletBaseController {
    private static final Logger LOGGER = Logger.getLogger(ServletPeopleNearbyController.class.getName());

    private static final Double RADIUS_IN_RANGE = 0.00299574131;

    private static final String PARAM_TOKEN = "token";
    private static final String PARAM_LOCATION = "location";
    private static final String PARAM_COORDS = "coords";
    private static final String PARAM_IN_CITY = "in_city";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("doGet");
        String token = req.getParameter(PARAM_TOKEN);
        String location = req.getParameter(PARAM_LOCATION);
        String coords = req.getParameter(PARAM_COORDS);
        String paramInCity = req.getParameter(PARAM_IN_CITY);

        User user = null;

        try {
            if (ValidateUtil.fullValidation(token) && ValidateUtil.fullValidation(location)
                    && ValidateUtil.fullValidation(coords)) {
                user = daoManager.selectUserByToken(token);
            } else {
                LOGGER.info("Invalid request parameters");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                return;
            }

            if (user == null) {
                LOGGER.info("Error authorization by token: " + token);
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_AUTHORIZATION);
                return;
            }
            Location place = Location.createFromWebSocketFormat(location, coords);
            if (place == null) {
                LOGGER.info("Can't parse the location");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                return;
            }
            if (!WebSocketUserInfoHandler.checkCompliance(place, user.getLogin())) {
                LOGGER.info(String.format("Discrepancy of location - %s to user - %s", location, user.getLogin()));
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.DISCREPANCY_OF_LOCATION);
                return;
            }
            List<WebSocketUserInfo> peoplesNearbyList;

            if ("true".equals(paramInCity)) {
                peoplesNearbyList = WebSocketUserInfoHandler
                        .getListOfWebSocketUserInfoInCity(place.getCountry(), place.getCity());
            } else {
                peoplesNearbyList = WebSocketUserInfoHandler.getListOfWebSocketUserInfoAtPlace(place);
                List<WebSocketUserInfo> tmpPeoplesNearbyList = new ArrayList<>();
                for (WebSocketUserInfo tmp : peoplesNearbyList) {
                    if (inRange(place.getCoords(), tmp.getLocation().getCoords())) {
                        tmpPeoplesNearbyList.add(tmp);
                    }
                }
                peoplesNearbyList = tmpPeoplesNearbyList;

            }
            peoplesNearbyList = peoplesNearbyList == null ? new ArrayList<WebSocketUserInfo>() : peoplesNearbyList;
            List<User> userList = new ArrayList<>();

            for (WebSocketUserInfo userInfo : peoplesNearbyList) {
                userList.add(userInfo.getUser());
            }
            SecureUtil.hideFieldsForUserListByFavorites(userList, daoManager.selectFavoriteUsers(user.getLogin()));

            resp.setContentType("json/text");
            resp.setCharacterEncoding("UTF-8");
            PrintWriter out = resp.getWriter();
            out.write(JsonUtil.webSocketUserInfoToJson(peoplesNearbyList));
        } catch (DaoSystemException e) {
            LOGGER.warn("Some dao error when select by token: " + token);
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.SYSTEM_ERROR);
        } catch (DaoBusinessException e) {
            LOGGER.info(e.getMessage());
            resp.sendRedirect((ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY));
        }
    }

    private boolean inRange(LocationCoords baseCoords, LocationCoords anotherCoords) {
        double radius = Math.sqrt(
                Math.pow(Math.abs(baseCoords.getLongitude() - anotherCoords.getLongitude()),2)
                + Math.pow(Math.abs(baseCoords.getLatitude() - anotherCoords.getLatitude()), 2));
        System.out.println(radius);

        return RADIUS_IN_RANGE.compareTo(radius) > 0;
    }
}
