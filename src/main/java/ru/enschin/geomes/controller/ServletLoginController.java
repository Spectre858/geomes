package ru.enschin.geomes.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import ru.enschin.geomes.dao.DaoManager;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.Error;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.util.JsonUtil;
import ru.enschin.geomes.util.ValidateUtil;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Andrew on 01.08.2016.
 */
public class ServletLoginController extends ServletBaseController {
    private Logger LOGGER = Logger.getLogger(ServletLoginController.class.getName());
    private static final String PARAM_TOKEN = "token";
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("doGet()");
        String token = req.getParameter(PARAM_TOKEN);
        String login = req.getParameter(PARAM_LOGIN);
        String password = req.getParameter(PARAM_PASSWORD);
        User user = null;

        try {
            if (ValidateUtil.fullValidation(token)) {
                user = daoManager.selectUserByToken(token);
            } else if (ValidateUtil.fullValidation(login) && ValidateUtil.fullValidation(password)) {
                user = daoManager.selectUserByLogin(login, password);
            } else {
                LOGGER.info("Invalid request parameters");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                return;
            }
            if (user == null) {
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY);
                return;
            }
            resp.setContentType("json/text");
            resp.setCharacterEncoding("UTF-8");
            PrintWriter out = resp.getWriter();
            String jsonResp = JsonUtil.userToJson(user);
            out.write(jsonResp);
        } catch (DaoSystemException e) {
            LOGGER.warn("Some error when select user in loginController with param: " +
                    "token - " + token + ", login: - " + login + ", password - " + password, e);
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.SYSTEM_ERROR);
        } catch (DaoBusinessException e) {
            LOGGER.info(e.getMessage());
            resp.sendRedirect((ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY));
        }
    }
}
