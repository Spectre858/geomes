package ru.enschin.geomes.controller;

import org.apache.log4j.Logger;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoNoSuchEntityException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.Error;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.util.SecureUtil;
import ru.enschin.geomes.util.JsonUtil;
import ru.enschin.geomes.util.ValidateUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Andrew on 02.08.2016.
 */
public class ServletCreateProfileController extends ServletBaseController {
    private static final Logger LOGGER = Logger.getLogger(ServletCreateProfileController.class.getName());

    private static final String PARAM_TOKEN = "token";
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_FULL_NAME = "full_name";
    private static final String PARAM_AGE = "age";
    private static final String PARAM_EMAIL = "e_mail";
    private static final String PARAM_PROFILE_PICTURE_LINK = "profile_picture_link";
    private static final String PARAM_TELEPHONE_NUMBER = "telephone_number";
    private static final String PARAM_VK_LINK = "vk_link";
    private static final String PARAM_FB_LINK = "fb_link";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("doGet()");
        User user = null;
        req.setCharacterEncoding("UTF-8");

        try {
            user = new User(req.getParameter(PARAM_TOKEN),
                    req.getParameter(PARAM_LOGIN),
                    req.getParameter(PARAM_PASSWORD),
                    null,
                    req.getParameter(PARAM_FULL_NAME),
                    Integer.valueOf(req.getParameter(PARAM_AGE)),
                    req.getParameter(PARAM_EMAIL),
                    req.getParameter(PARAM_PROFILE_PICTURE_LINK),
                    req.getParameter(PARAM_TELEPHONE_NUMBER),
                    req.getParameter(PARAM_VK_LINK),
                    req.getParameter(PARAM_FB_LINK));


            while (true) {
                user.setToken(SecureUtil.getTokenByLogin(user.getLogin()));
                try {
                    daoManager.selectUserByToken(user.getToken());
                } catch (DaoNoSuchEntityException e) {
                    break;
                }
            }

            if (!ValidateUtil.validateOfUser(user)) {
                LOGGER.info("Invalid request parameters");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                return;
            }

            daoManager.createUser(user);
//            EmailUtil.sendMessage(user.geteMail(), EmailUtil.DEFAULT_SUBJECTS.USER_REGISTRATION,
//                    EmailUtil.DEFAULT_MESSAGES.USER_REGISTRATION);

            LOGGER.info("Created new user: " + user);

            resp.setContentType("json/text");
            resp.setCharacterEncoding("UTF-8");
            PrintWriter out = resp.getWriter();
            out.write(JsonUtil.tokenToJson(user.getToken()));
        } catch (DaoBusinessException e) {
            LOGGER.info(e.getMessage());
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.ENTITY_ALREADY_EXIST);
        } catch (DaoSystemException e) {
            LOGGER.warn(e);
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.SYSTEM_ERROR);
        } /*catch (MessagingException e) {
            LOGGER.info("Can't send the message - Error message : " + e.getMessage());
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_EMAIL);
        } */catch (Exception e) {
            LOGGER.warn("Invalid request parameters: " + user, e);
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
        }

    }
}
