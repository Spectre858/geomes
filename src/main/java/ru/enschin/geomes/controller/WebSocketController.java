package ru.enschin.geomes.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import ru.enschin.geomes.dao.DaoManager;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoNoSuchEntityException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.inject.FieldReflector;
import ru.enschin.geomes.inject.Inject;
import ru.enschin.geomes.model.*;
import ru.enschin.geomes.util.*;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;


/**
 * Created by Andrew on 03.08.2016.
 */
@Scope("application")
@ServerEndpoint("/chat/{user}/{place}/{longlat}")
public class WebSocketController {
    private static final Logger LOGGER = Logger.getLogger(WebSocketController.class.getName());
    private static final int COUNT_MESSAGE_LIMIT = Integer.MAX_VALUE;
    private static final long LIMIT_LIFETIME_MILLISECONDS = 1000 * 60 * 60 * 6;

    @Inject("daoManager")
    private DaoManager daoManager;

    @OnMessage
    public void onMessage(String message, Session session, @PathParam("user") String login,
                          @PathParam("place") String place, @PathParam("longlat") String longlat) {
        LOGGER.info("onMessage : " + String.format("from user - {%s} -> %s - [%s]", login, place, longlat));
        try {
            User user = getValidUser(login);
            if (user == null) {
                sendError(session, WebSocketMessage.Type.INVALID_AUTHORIZATION_ERROR, null);
                return;
            }
            Location location = getValidLocation(place, longlat);
            if (location == null) {
                sendError(session, WebSocketMessage.Type.INVALID_REQUEST_PARAMETERS, "Can't parse the location");
                return;
            }

            if (!(ValidateUtil.fullValidation(message) && ValidateUtil.isJson(message))) {
                sendError(location, login, WebSocketMessage.Type.INVALID_MESSAGE_ERROR, "");
                return;
            }
            WebSocketMessage parsedMessage = JsonUtil.jsonToWebSocketMessage(message);
            if (!ValidateUtil.validateOfWebSocketMessage(parsedMessage)) {
                sendError(location, login, WebSocketMessage.Type.INVALID_MESSAGE_ERROR, "");
                return;
            }
            if (WebSocketMessage.Type.MESSAGE.compareTo(parsedMessage.getType()) == 0) {
                DialogInfo dialogInfo = daoManager.selectUserDialogInfo(login, place, parsedMessage.getDestinationUser());
                if (dialogInfo == null) {
                    dialogInfo =
                            new DialogInfo(place, login, parsedMessage.getDestinationUser(), 0, null);
                }
                long timeDifference = (dialogInfo.getLastMessageTime() == null)
                        ? LIMIT_LIFETIME_MILLISECONDS : DateUtil.getDifference(
                        DateUtil.getCurrentDate(), DateUtil.parse(dialogInfo.getLastMessageTime()));
                if ((dialogInfo.getCountMessage() >= COUNT_MESSAGE_LIMIT)
                        && (timeDifference < LIMIT_LIFETIME_MILLISECONDS)) {
                    sendError(location, login, WebSocketMessage.Type.LIMIT_MESSAGE_ERROR, "");
                } else {
                    if (dialogInfo.getCountMessage() >= COUNT_MESSAGE_LIMIT) {
                        dialogInfo.setCountMessage(0);
                    }
                    WebSocketUserInfoHandler
                            .sendMessage(location, parsedMessage.getDestinationUser(), message);
                    daoManager.updateUserDialog(login, place, parsedMessage.getDestinationUser(),
                            dialogInfo.getCountMessage() + 1, DateUtil.getCurrentDateAsString());
                }
            } else if (WebSocketMessage.Type.UPDATE_COORDS.equals(parsedMessage.getType())) {
                LocationCoords newCoords = LocationCoords.createFromWebSocketFormat(parsedMessage.getMessage());
                WebSocketUserInfoHandler.updateCoords(location, user.getLogin(), newCoords);
            }
        } catch (IOException e) {
            LOGGER.warn("some IO error in onMessage", e);
        } catch (NullPointerException e) {
            LOGGER.info("user with such login does not exist in your location - " + place, e);
            sendError(session, WebSocketMessage.Type.NO_SUCH_DESTINATION_ERROR, "");
        } catch (DaoBusinessException e) {
            LOGGER.info("such owner does not exist", e);
            sendError(session, WebSocketMessage.Type.INVALID_AUTHORIZATION_ERROR, "");
        } catch (DaoSystemException e) {
            LOGGER.info("some system error in onMessage in dao", e);
            sendError(session, WebSocketMessage.Type.SYSTEM_ERROR, "");
        }
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("user") String login, @PathParam("place") String place,
                       @PathParam("longlat") String longlat) {
        LOGGER.info("Client connected:" +  String.format("from user - {%s} -> %s - [%s]", login, place, longlat));

        try {
            injectField();
            User user = getValidUser(login);

            if (user == null) {
                sendError(session, WebSocketMessage.Type.INVALID_AUTHORIZATION_ERROR, "");
                return;
            }
            Location location = getValidLocation(place, longlat);

            if (location == null) {
                sendError(session, WebSocketMessage.Type.INVALID_REQUEST_PARAMETERS, "can't parse the location");
                return;
            }
            WebSocketUserInfoHandler.addWebSocketUserInfo(user, location, session);
        } catch (DaoSystemException e) {
            LOGGER.info("Dao exception in onOpen" + e.getMessage());
            sendError(session, WebSocketMessage.Type.INVALID_AUTHORIZATION_ERROR, "");
        } catch (DaoBusinessException e) {
            sendError(session, WebSocketMessage.Type.INVALID_AUTHORIZATION_ERROR, "Such user already login");
        } catch (Exception e) {
            LOGGER.warn("Can't inject in onOpen", e);
            sendError(session, WebSocketMessage.Type.SYSTEM_ERROR, "");
        }
    }


    @OnClose
    public void onClose(@PathParam("user") String login, @PathParam("place") String place,
                        @PathParam("longlat") String longlat) {
        LOGGER.info("Client disconnected: place - " + String.format("from user - {%s} -> %s - [%s]", login, place, longlat));
        try {
            User user = getValidUser(login);

            if (user == null) {
                return;
            }
            Location location = getValidLocation(place, longlat);

            if (location == null) {
                return;
            }
            WebSocketUserInfoHandler.removeWebSocketUserInfo(location, login);
        } catch (Exception e) {
            LOGGER.warn(String.format("Some error in onClose(session, %s, %s)", login, place));
        }
    }

    @OnError
    public void onError(Session session, Throwable thr) {
        LOGGER.warn("onError: Something go wrong - " + thr.getMessage());
    }

    @Deprecated
    private void sendError(String location, String destinationUser,
                           WebSocketMessage.Type errorType, String errorMessage)
            throws IOException {

        errorMessage = (errorMessage == null) ? "" : errorMessage;
        WebSocketMessage resp = new WebSocketMessage(errorType, destinationUser, WebSocketMessage.SOURCE_USER_SERVER, errorMessage);
        WebSocketUserInfoHandler
                .sendMessage(location, destinationUser, JsonUtil.webSocketMessageToJson(resp));
    }
    private void sendError(Session session, WebSocketMessage.Type errorType, String errorMessage) {
        LOGGER.info("sendError");
        errorMessage = (errorMessage == null) ? "" : errorMessage;
        WebSocketMessage resp = new WebSocketMessage(errorType, WebSocketMessage.DESTINATION_USER_DEFAULT,
                WebSocketMessage.SOURCE_USER_SERVER, errorMessage);
        try {
            session.getBasicRemote().sendText(JsonUtil.webSocketMessageToJson(resp));
        } catch (IOException e) {
            LOGGER.info("sendError - fail");
        }
    }

    private void sendError(Location location, String destinationUser,
                           WebSocketMessage.Type errorType, String errorMessage) {
        LOGGER.info("sendError");
        errorMessage = (errorMessage == null) ? "" : errorMessage;
        WebSocketMessage resp = new WebSocketMessage(errorType, destinationUser,
                WebSocketMessage.SOURCE_USER_SERVER, errorMessage);
        try {
            WebSocketUserInfoHandler.sendMessage(location, destinationUser, JsonUtil.webSocketMessageToJson(resp));
        } catch (DaoNoSuchEntityException | IOException e) {
            LOGGER.info("sendError - fail");
        }
    }

    private User getValidUser(String login) throws DaoSystemException, DaoBusinessException {
        if (!ValidateUtil.fullValidation(login)) {
            return null;
        }
        return daoManager.selectUserInfoByLogin(login);
    }

    private Location getValidLocation(String place, String longlat) {
        if (!ValidateUtil.fullValidation(place) || !ValidateUtil.fullValidation(longlat)) {
            return null;
        }
        return Location.createFromWebSocketFormat(place, longlat);
    }

    private void injectField() throws Exception {
        try {
            LOGGER.info("injecting fields");
            ApplicationContext appCtx = ApplicationContextUtil.getDefaultInstanceOfApplicationContext();
            List<Field> allFields = FieldReflector.collectUpTo(this.getClass(), Object.class);
            List<Field> injectFields = FieldReflector.filterInject(allFields);

            for (Field field : injectFields) {
                field.setAccessible(true);
                Inject annotation = field.getAnnotation(Inject.class);
                Object bean = appCtx.getBean(annotation.value());
                if (bean == null) {
                    LOGGER.warn("There isn't bean with such name");
                    throw new Exception("There isn't bean with such name");
                }
                field.set(this, bean);
            }
        } catch (Exception e) {
            LOGGER.warn("Can't inject", e);
            throw new Exception("Can't inject", e);
        }
    }

}
