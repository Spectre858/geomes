package ru.enschin.geomes.controller;

import org.apache.log4j.Logger;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.Error;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.util.SecureUtil;
import ru.enschin.geomes.util.JsonUtil;
import ru.enschin.geomes.util.ValidateUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrew on 15.08.2016.
 */
public class ServletUserProfileListController extends ServletBaseController {
    private static final Logger LOGGER = Logger.getLogger(ServletUserProfileListController.class.getName());
    private static final String PARAM_TOKEN = "token";
    private static final String PARAM_LOGINS = "logins";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String loginsString = req.getParameter(PARAM_LOGINS);
        String token = req.getParameter(PARAM_TOKEN);

        try {
            if (!ValidateUtil.fullValidation(loginsString) || !ValidateUtil.fullValidation(token)) {
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                return;
            }
            User owner = daoManager.selectUserByToken(token);
            if (owner == null) {
                LOGGER.info("error authorization by token");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_AUTHORIZATION);
                return;
            }
            String[] separetedLogins = loginsString.split(",");

            List<User> users = daoManager.selectListOfUserInfoByListOfLogins(Arrays.asList(separetedLogins));
            SecureUtil.hideFieldsForUserListByFavorites(users, daoManager.selectFavoriteUsers(owner.getLogin()));
            resp.setContentType("json/text");
            resp.setCharacterEncoding("UTF-8");
            PrintWriter out = resp.getWriter();
            out.write(JsonUtil.userListToJson(users));
        } catch (DaoSystemException e) {
            LOGGER.warn("Some dao error when select list of user info by list of logins: ");
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.SYSTEM_ERROR);
        } catch (DaoBusinessException e) {
            LOGGER.info(e.getMessage());
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY);
        }
    }
}
