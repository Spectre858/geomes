package ru.enschin.geomes.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import ru.enschin.geomes.dao.DaoManager;
import ru.enschin.geomes.inject.FieldReflector;
import ru.enschin.geomes.inject.Inject;
import ru.enschin.geomes.util.ApplicationContextUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.lang.reflect.Field;
import java.util.List;

/*
       todo global: 1) done
       todo global: 2) Сделать систему слежения за фолдами (отправлять на почту)
       todo global: 3) Сделать систему, которая будет отправлять логи на почту по определенному интервалу
 */

public class ServletBaseController extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(ServletBaseController.class.getName());

    private static final String APP_CTX_PATH = "contextConfigLocation";

    public static final String ERROR_TYPE_PARAM = "?error_type=";
    public static final String ERROR_PAGE = "/geomes-0.1.STARTUP/error";

    @Inject("daoManager")
    DaoManager daoManager;

    @Override
    public void init() throws ServletException {
        super.init();

        try {
            ApplicationContext appCtx = ApplicationContextUtil.getInstanceOfApplicationContext(this.getServletContext());
            List<Field> allFields = FieldReflector.collectUpTo(this.getClass(), HttpServlet.class);
            List<Field> injectFields = FieldReflector.filterInject(allFields);

            for (Field field : injectFields) {
                field.setAccessible(true);
                Inject annotation = field.getAnnotation(Inject.class);
                Object bean = appCtx.getBean(annotation.value());
                if (bean == null) {
                    LOGGER.warn("There isn't bean with such name");
                    throw new ServletException("There isn't bean with such name");
                }
                field.set(this, bean);
            }
        } catch (Exception e) {
            LOGGER.warn("Can't inject from " + APP_CTX_PATH, e);
            throw new ServletException("Can't inject from " + APP_CTX_PATH, e);
        }
    }
}
