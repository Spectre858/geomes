package ru.enschin.geomes.controller;

import org.apache.log4j.Logger;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.Error;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.util.JsonUtil;
import ru.enschin.geomes.util.ValidateUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Andrew on 16.09.2016.
 */
public class ServletUserMoodController extends ServletBaseController {
    private static final Logger LOGGER = Logger.getLogger(ServletUserMoodController.class.getName());
    private static final String PARAM_TOKEN = "token";
    private static final String PARAM_MOOD = "mood";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("Do GET");
        String token = req.getParameter(PARAM_TOKEN);
        String mood = req.getParameter(PARAM_MOOD);
        User user = null;

        try {
            if (!ValidateUtil.fullValidation(token) || !ValidateUtil.fullValidation(mood)) {
                LOGGER.info("Invalid request parameters");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                return;
            }
            user = daoManager.selectUserByToken(token);
            if (user == null) {
                LOGGER.info("Invalid authorization");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_AUTHORIZATION);
                return;
            }

            daoManager.updateUserMood(token, mood);

            resp.setContentType("json/text");
            resp.setCharacterEncoding("UTF-8");
            PrintWriter out = resp.getWriter();
            out.write(JsonUtil.getSuccessJson());
        } catch (DaoSystemException e) {
            LOGGER.warn("Some dao error when update user mood: ", e);
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.SYSTEM_ERROR);
        } catch (DaoBusinessException e) {
            LOGGER.info(e.getMessage());
            resp.sendRedirect((ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY));
        }
    }
}
