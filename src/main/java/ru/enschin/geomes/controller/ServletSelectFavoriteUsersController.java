package ru.enschin.geomes.controller;

import org.apache.log4j.Logger;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.Error;
import ru.enschin.geomes.model.Favorites;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.util.SecureUtil;
import ru.enschin.geomes.util.JsonUtil;
import ru.enschin.geomes.util.ValidateUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Andrew on 23.08.2016.
 */
public class ServletSelectFavoriteUsersController extends ServletBaseController {
    private static final Logger LOGGER = Logger.getLogger(ServletSelectFavoriteUsersController.class.getName());
    private static final String PARAM_TOKEN = "token";
    private static final String PARAM_USER = "user";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("doGet");
        String token = req.getParameter(PARAM_TOKEN);
        String login = req.getParameter(PARAM_USER);

        User owner = null;
        User user = null;

        try {
            if (!ValidateUtil.fullValidation(token) || !ValidateUtil.fullValidation(login)) {
                LOGGER.info("Invalid request parameters");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                return;
            }
            owner = daoManager.selectUserByToken(token);
            if (owner.getLogin().compareTo(login) == 0) {
                user = owner;
            } else {
                user = daoManager.selectUserInfoByLogin(login);
            }
            if (owner == null || user == null) {
                LOGGER.info("No such entity");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY);
                return;
            }

            Favorites favorites = daoManager.selectFavoriteUsers(user.getLogin());

            SecureUtil.hideFieldsForUserListByFavorites(favorites.getFavoriteUsers(),
                    daoManager.selectFavoriteUsers(owner.getLogin()));

            resp.setContentType("json/text");
            resp.setCharacterEncoding("UTF-8");
            PrintWriter out = resp.getWriter();
            out.write(JsonUtil.favoritesToJson(favorites));

        } catch (DaoSystemException e) {
            LOGGER.warn("Some dao error in select favorite user: ");
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.SYSTEM_ERROR);
        } catch (DaoBusinessException e) {
            LOGGER.info(e.getMessage());
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY);
        }
    }
}
