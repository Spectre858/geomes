package ru.enschin.geomes.controller;

import org.apache.log4j.Logger;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.Error;
import ru.enschin.geomes.model.Location;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.model.WebSocketMessage;
import ru.enschin.geomes.util.SecureUtil;
import ru.enschin.geomes.util.JsonUtil;
import ru.enschin.geomes.util.ValidateUtil;
import ru.enschin.geomes.util.WebSocketUserInfoHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Andrew on 21.08.2016.
 */
public class ServletPutTheLikeController extends ServletBaseController {
    private static final Logger LOGGER = Logger.getLogger(ServletPutTheLikeController.class.getName());
    private static final String PARAM_TOKEN = "token";
    private static final String PARAM_USER = "user";
    private static final String PARAM_LOCATION = "location";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("doGet");
        String token = req.getParameter(PARAM_TOKEN);
        String user = req.getParameter(PARAM_USER);
        String location = req.getParameter(PARAM_LOCATION);

        User owner = null;
        User likedUser = null;

        try {
            if (!ValidateUtil.fullValidation(token) || !ValidateUtil.fullValidation(user)
                    || !ValidateUtil.fullValidation(location)) {
                LOGGER.info("Invalid request parameters");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                return;
            } else {
                owner = daoManager.selectUserInfoByLogin(user);
                likedUser = daoManager.selectUserByToken(token);
            }
            if (owner == null || likedUser == null) {
                LOGGER.info("No such entity");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY);
                return;
            }
            Location place = Location.createByPlace(location);
            if (place == null) {
                LOGGER.info("Can't parse the location");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                return;
            }
            if (!WebSocketUserInfoHandler.checkCompliance(place, likedUser.getLogin())) {
                LOGGER.info(String.format("Discrepancy of location - %s to user - %s", location, likedUser.getLogin()));
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.DISCREPANCY_OF_LOCATION);
                return;
            }
            daoManager.insertLikeByUserId(owner.getLogin(), likedUser.getId());
            SecureUtil.hideFieldsForUserByFavorites(likedUser, daoManager.selectFavoriteUsers(owner.getLogin()));
            WebSocketMessage message =
                    new WebSocketMessage(WebSocketMessage.Type.LIKE, owner.getLogin(), likedUser.getLogin(), JsonUtil.userToJson(likedUser));
            WebSocketUserInfoHandler.sendMessage(place, owner.getLogin(), JsonUtil.webSocketMessageToJson(message));

            resp.setContentType("json/text");
            resp.setCharacterEncoding("UTF-8");
            PrintWriter out = resp.getWriter();
            out.write(JsonUtil.getSuccessJson());

        } catch (DaoSystemException e) {
            LOGGER.warn("Some dao error in put like: ");
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.SYSTEM_ERROR);
        } catch (DaoBusinessException e) {
            LOGGER.info(e.getMessage());
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY);
        }
    }
}
