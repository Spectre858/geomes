package ru.enschin.geomes.controller;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.Error;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.util.ValidateUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;

public class ServletFileUploadController extends ServletBaseController {
    private static final Logger LOGGER = Logger.getLogger(ServletFileUploadController.class.getName());
    private static final String PARAM_TOKEN = "token";
    private static final String PHOTO_DIR_LOCATION = "photoDirLocation";
    private static final String PHOTO_URI = "/image/";

    private Random random = new Random();


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("doPost()");
        User user = null;

        if (!ServletFileUpload.isMultipartContent(req)) {
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_TYPE);
            return;
        }

        String token = req.getParameter(PARAM_TOKEN);
        if (!ValidateUtil.fullValidation(token)) {
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
            return;
        }

        try {
            user = daoManager.selectUserByToken(token);
            if (user == null) {
                LOGGER.info("error authorization by token: " + token);
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_AUTHORIZATION);
                return;
            }

            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(1024*1024);

            File tempDir = (File)getServletContext().getAttribute("javax.servlet.context.tempdir");
            factory.setRepository(tempDir);

            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(1024 * 1024 * 10);

            List<FileItem> items = upload.parseRequest(req);


            for (FileItem item : items) {
                if (item.isFormField()) {
//                    processFormField(item); while ignore
                } else {
                    if (!item.getContentType().startsWith("image")) {
                        resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_TYPE);
                        return;
                    }
                    if (item.getSize() > 10 * 1024 * 1024) {
                        resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                        return;
                    }
                    String fileName = processUploadedFile(item, user);
                    StringBuffer url = req.getRequestURL();
                    url = url
                            .delete(url.lastIndexOf("/"), url.length())
                            .append(PHOTO_URI)
                            .append(fileName);

                    resp.setContentType("json/text");
                    resp.setCharacterEncoding("UTF-8");
                    PrintWriter out = resp.getWriter();
                    out.write(url.toString());
                }
            }

        } catch (DaoSystemException e) {
            LOGGER.warn("Some system error in dao", e);
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.SYSTEM_ERROR);
        } catch (FileUploadException e) {
            LOGGER.info("Can't to parse the request", e);
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_TYPE);
        } catch (Exception e) {
            LOGGER.info("Can't write in file" , e);
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.SYSTEM_ERROR);
        }

    }

    private String processUploadedFile(FileItem item, User user) throws Exception {
        File uploadedFile = null;

        String dir = getServletContext().getInitParameter(PHOTO_DIR_LOCATION);
//        String dir = getServletContext().getInitParameter(ROOT_DIR_LOCATION);
//        dir = getServletContext().getRealPath(dir);
//        StringBuilder buf = new StringBuilder(dir);
//        buf.deleteCharAt(buf.length() - 1)
//                .delete(buf.lastIndexOf("\\"), buf.length())
//                .append(getServletContext().getInitParameter(PHOTO_DIR_LOCATION));
//        dir = buf.toString();

        File catalog = new File(dir);
        if (!catalog.exists()) {
            catalog.mkdir();
            LOGGER.info("Create dir: " + dir);
        }

        do {
            String path = dir + user.getId() + user.getLogin()
                    + Math.abs(random.nextInt()) + item.getContentType().replace("/", ".");
            uploadedFile = new File(path);
        } while (uploadedFile.exists());

        uploadedFile.createNewFile();
        LOGGER.info("PATH: " + uploadedFile.getAbsolutePath());
        item.write(uploadedFile);
        return uploadedFile.getName();
    }

    private void processFormField(FileItem item) {
        LOGGER.info(item.getFieldName() + " = " + item.getString());
    }
}
