package ru.enschin.geomes.controller;

import org.apache.log4j.Logger;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.*;
import ru.enschin.geomes.model.Error;
import ru.enschin.geomes.util.SecureUtil;
import ru.enschin.geomes.util.JsonUtil;
import ru.enschin.geomes.util.ValidateUtil;
import ru.enschin.geomes.util.WebSocketUserInfoHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Andrew on 23.08.2016.
 */
public class ServletInsertFavoriteUserController extends ServletBaseController {
    private static final Logger LOGGER = Logger.getLogger(ServletInsertFavoriteUserController.class.getName());
    private static final String PARAM_TOKEN = "token";
    private static final String PARAM_FAVORITE_USER = "user";
    private static final String PARAM_LOCATION = "location";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("doGet");
        String token = req.getParameter(PARAM_TOKEN);
        String user = req.getParameter(PARAM_FAVORITE_USER);
        String location = req.getParameter(PARAM_LOCATION);

        User owner = null;
        User favoriteUser = null;

        try {
            if (!ValidateUtil.fullValidation(token) || !ValidateUtil.fullValidation(user)
                    || !ValidateUtil.fullValidation(location)) {
                LOGGER.info("Invalid request parameters");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                return;
            }
            owner = daoManager.selectUserByToken(token);
            favoriteUser = daoManager.selectUserInfoByLogin(user);
            if (owner == null || favoriteUser == null) {
                LOGGER.info("No such entity");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY);
                return;
            }
            Location place = Location.createByPlace(location);
            if (place == null) {
                LOGGER.info("Can't parse the location");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
                return;
            }
            if (!WebSocketUserInfoHandler.checkCompliance(place, owner.getLogin())) {
                LOGGER.info(String.format("Discrepancy of location - %s to user - %s", location, owner.getLogin()));
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.DISCREPANCY_OF_LOCATION);
                return;
            }

            daoManager.insertFavoriteUser(owner.getId(), favoriteUser.getLogin());
            SecureUtil.hideFieldsForUserByFavorites(owner, daoManager.selectFavoriteUsers(favoriteUser.getLogin()));
            WebSocketMessage message =
                    new WebSocketMessage(WebSocketMessage.Type.ADDITION_IN_FAVORITE, favoriteUser.getLogin(),
                            owner.getLogin(), JsonUtil.userToJson(owner));
            WebSocketUserInfoHandler.sendMessage(place, favoriteUser.getLogin(), JsonUtil.webSocketMessageToJson(message));

            resp.setContentType("json/text");
            resp.setCharacterEncoding("UTF-8");
            PrintWriter out = resp.getWriter();
            out.write(JsonUtil.getSuccessJson());

        } catch (DaoSystemException e) {
            LOGGER.warn("Some dao error in insert favorite user: ");
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.SYSTEM_ERROR);
        } catch (DaoBusinessException e) {
            LOGGER.info(e.getMessage());
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY);
        }
    }
}
