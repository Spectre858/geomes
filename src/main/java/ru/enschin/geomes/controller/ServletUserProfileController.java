package ru.enschin.geomes.controller;

import org.apache.log4j.Logger;
import ru.enschin.geomes.dao.exception.DaoBusinessException;
import ru.enschin.geomes.dao.exception.DaoSystemException;
import ru.enschin.geomes.model.Error;
import ru.enschin.geomes.model.User;
import ru.enschin.geomes.util.SecureUtil;
import ru.enschin.geomes.util.JsonUtil;
import ru.enschin.geomes.util.ValidateUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Andrew on 03.08.2016.
 */
public class ServletUserProfileController extends ServletBaseController {
    private static final Logger LOGGER = Logger.getLogger(ServletUserProfileController.class.getName());

    private static final String PARAM_MY_TOKEN = "my_token";
    private static final String PARAM_LOGIN = "login";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("doGet()");

        String myToken = req.getParameter(PARAM_MY_TOKEN);
        String login = req.getParameter(PARAM_LOGIN);

        User owner = null;
        User user = null;

        try {
            if (ValidateUtil.fullValidation(myToken) && ValidateUtil.fullValidation(login)) {
                owner = daoManager.selectUserByToken(myToken);
                if (owner == null) {
                    LOGGER.info("error authorization by token: " + myToken);
                    resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_AUTHORIZATION);
                    return;
                }
                if (owner.getLogin().compareTo(login) != 0) {
                    user = daoManager.selectUserInfoByLogin(login);
                    if (user == null) {
                        resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY);
                        return;
                    }
                    SecureUtil.hideFieldsForUserByFavorites(user, daoManager.selectFavoriteUsers(owner.getLogin()));
                } else {
                    user = owner;
                }
                resp.setContentType("json/text");
                resp.setCharacterEncoding("UTF-8");
                PrintWriter out = resp.getWriter();
                out.write(JsonUtil.userToJson(user));
            } else {
                LOGGER.info("Invalid request parameters");
                resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.INVALID_REQUEST_PARAMETERS);
            }
        } catch (DaoSystemException e) {
            LOGGER.warn("some error when trying to get user info from db", e);
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.SYSTEM_ERROR);
        } catch (DaoBusinessException e) {
            LOGGER.info(e.getMessage());
            resp.sendRedirect(ERROR_PAGE + ERROR_TYPE_PARAM + Error.Type.NO_SUCH_ENTITY);
        }

    }
}
